# breadth_first_search.py
"""Volume 2: Breadth-First Search.
<Name>
<Class>
<Date>
"""
import numpy as np
from collections import deque
import random
import networkx as nx
from matplotlib import pyplot as plt

# Problems 1-3
class Graph:
    """A graph object, stored as an adjacency dictionary. Each node in the
    graph is a key in the dictionary. The value of each key is a set of
    the corresponding node's neighbors.

    Attributes:
        d (dict): the adjacency dictionary of the graph.
    """
    def __init__(self, adjacency={}):
        """Store the adjacency dictionary as a class attribute"""
        self.d = dict(adjacency)

    def __str__(self):
        """String representation: a view of the adjacency dictionary."""
        return str(self.d)

    # Problem 1
    def add_node(self, n):
        """Add n to the graph (with no initial edges) if it is not already
        present.

        Parameters:
            n: the label for the new node.
        """
        if n in self.d:
            raise ValueError("Key already present")
        self.d.update({n:set()})
        return self

    # Problem 1
    def add_edge(self, u, v):
        """Add an edge between node u and node v. Also add u and v to the graph
        if they are not already present.

        Parameters:
            u: a node label.
            v: a node label.
        """
        
        if u not in self.d:  #Add u and v if they're not in the graph
            self.add_node(u)
        if v not in self.d:
            self.add_node(v)
        
        self.d[u].add(v)    #Add u and v to eachother's entries
        self.d[v].add(u)
        return self
        
    # Problem 1
    def remove_node(self, n):
        """Remove n from the graph, including all edges adjacent to it.

        Parameters:
            n: the label for the node to remove.

        Raises:
            KeyError: if n is not in the graph.
        """
        
        if n not in self.d:
            raise KeyError(n+" is not in graph")
        del self.d[n]
        for i in self.d.keys():   #Comb through the dictionary entries and remove n
            if n in self.d[i]:
                self.d[i].remove(n) 
        return self


    # Problem 1
    def remove_edge(self, u, v):
        """Remove the edge between nodes u and v.

        Parameters:
            u: a node label.
            v: a node label.

        Raises:
            KeyError: if u or v are not in the graph, or if there is no
                edge between u and v.
        """

        if u not in self.d[v] or v not in self.d[u]:
            raise KeyError(u+" and "+v+" do not share an edge")

        self.d[v].remove(u) 
        self.d[u].remove(v)

        return self


    # Problem 2
    def traverse(self, source):
        """Traverse the graph with a breadth-first search until all nodes
        have been visited. Return the list of nodes in the order that they
        were visited.

        Parameters:
            source: the node to start the search at.

        Returns:
            (list): the nodes in order of visitation.

        Raises:
            KeyError: if the source node is not in the graph.
        """
        self.list = []
        
        if source not in self.d:
            raise KeyError(source+" is not in the graph.")
        
        current = source #Start with source
        tovisit = deque()
        while True:
            for i in self.d[current]:
                if i not in tovisit and i not in self.list:
                    tovisit.append(i)
            if current not in self.list:
                self.list.append(current)
            if len(tovisit) == 0:
                break                
            current = tovisit.popleft()
        return self.list
        
        

    # Problem 3
    def shortest_path(self, source, target):
        """Begin a BFS at the source node and proceed until the target is
        found. Return a list containing the nodes in the shortest path from
        the source to the target, including endoints.

        Parameters:
            source: the node to start the search at.
            target: the node to search for.

        Returns:
            A list of nodes along the shortest path from source to target,
                including the endpoints.

        Raises:
            KeyError: if the source or target nodes are not in the graph.
        """
        
        self.list = []
        
        if source not in self.d:
            raise KeyError(source+" is not in the graph.")
        if target not in self.d:
            raise KeyError(target+" is not in graph")


        M = set(source)   #shortest path
        S = [[source]]    #queue
        while S:
            path = S.pop(0)
            M.add(path[-1])
            if path[-1] == target:
                return path
            for node in self.d[path[-1]] - M:
                S.append(path+[node])
        return M   




# Problems 4-6
class MovieGraph:
    """Class for solving the Kevin Bacon problem with movie data from IMDb."""

    # Problem 4
    def __init__(self, filename="movie_data.txt"):
        """Initialize a set for movie titles, a set for actor names, and an
        empty NetworkX Graph, and store them as attributes. Read the speficied
        file line by line, adding the title to the set of movies and the cast
        members to the set of actors. Add an edge to the graph between the
        movie and each cast member.

        Each line of the file represents one movie: the title is listed first,
        then the cast members, with entries separated by a '/' character.
        For example, the line for 'The Dark Knight (2008)' starts with

        The Dark Knight (2008)/Christian Bale/Heath Ledger/Aaron Eckhart/...

        Any '/' characters in movie titles have been replaced with the
        vertical pipe character | (for example, Frost|Nixon (2008)).
        """
        while True:
           try:
               with open(filename,encoding='utf-8') as infile:
                   contents=infile.read()
               break  
           except (FileNotFoundError, FileExistsError, IOError, SyntaxError):
               infile=input("Please input a valid filename:")
        
        contents = contents.strip('\n').split('\n')
        self.movie_titles = set() #Initialize
        self.actor_names = set()
        self.graph = nx.Graph()
        
        for line in contents:
            line = line.strip('/').split('/') #Step throuh each line
            title=line.pop(0)  #Assign movie title
            self.movie_titles.add(title)
            self.actor_names.update(line)
            self.graph.add_edges_from([(title,name) for name in line])
        
    # Problem 5
    def path_to_actor(self, source, target):
        """Compute the shortest path from source to target and the degrees of
        separation between source and target.

        Returns:
            (list): a shortest path from source to target, including endpoints and movies.
            (int): the number of steps from source to target, excluding movies.
        """
        if nx.has_path(self.graph, source, target) == False:
            raise ValueError("No path exists between "+source+" and "+target)
        
        shortest = nx.shortest_path(self.graph,source,target)
        
        length = nx.shortest_path_length(self.graph,source,target)
        
        pathactors = length - length//2
        
        return shortest, pathactors

    # Problem 6
    def average_number(self, target):
        """Calculate the shortest path lengths of every actor to the target
        (not including movies). Plot the distribution of path lengths and
        return the average path length.

        Returns:
            (float): the average path length from actor to target.
        """
        
 	
        lengthsall = nx.single_source_shortest_path_length(self.graph,target)
        lengths = []
        for actor in self.actor_names:
            lengths.append(lengthsall[actor]/2)
        
        plt.hist(lengths,bins=[i-.5 for i in range(8)],color='k')
        plt.title("Length of path from "+target+" and all other actors")
        plt.xlabel("Length of path")
        plt.ylabel("Number of paths")
        plt.show()
        
        print(sum(lengths)/len(lengths))

def testgraph():
    graph = Graph()
    graph.add_node('A')
    graph.add_node('B')
    graph.add_node('C')
    graph.add_node('E')
    graph.add_node('F')
    graph.add_edge('A', 'B')
    graph.add_edge('B','D')
    graph.add_edge('B','C')
    graph.add_edge('C','A')
    graph.add_edge('C','E')
    graph.add_edge('A','D')
    graph.add_edge('A','B')
    graph.add_edge('C','D')
    graph.add_edge('A','E')
    graph.add_edge('E','F')
    print(graph,'\n')
    # graph.remove_edge('C','B')
    # graph.remove_edge('D','A')
    # print(graph)
    return graph

def randomgraph():
    import string
    alph = list(string.ascii_uppercase)
    alphedge = list(string.ascii_uppercase)
    graph = Graph()
    while len(alph) != 12:  
        graph.add_node(alph.pop(random.randint(0,len(alph)-1)))
    for i in range(500):
        graph.add_edge(alphedge[random.randint(0,25)],alphedge[random.randint(0,25)])
    return(graph)

def bfsgraph():
    graph=Graph()
    graph.add_node('A')
    graph.add_node('B')
    graph.add_node('C')
    graph.add_node('D')
    graph.add_edge('A','D')
    graph.add_edge('A','B')
    graph.add_edge('B','D')
    graph.add_edge('C','D')
    return graph

def bfsgraph2():
    graph=Graph()
    graph.add_edge('A','F')
    graph.add_edge('A','B')
    graph.add_edge('B','A')
    graph.add_edge('B','C')
    graph.add_edge('C','D')
    graph.add_edge('C','B')
    graph.add_edge('D','E')
    graph.add_edge('D','C')
    graph.add_edge('E','F')
    graph.add_edge('E','D')
    graph.add_edge('F','G')
    graph.add_edge('F','A')
    graph.add_edge('F','E')
    graph.add_edge('G','F')
    return graph