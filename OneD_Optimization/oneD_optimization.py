# oneD_optimization.py
"""Volume 2: One-Dimensional Optimization.
<Name>
<Class>
<Date>
"""
import numpy as np
from scipy import optimize as opt
import sympy as sy
from scipy.optimize import linesearch
from autograd import numpy as anp
from autograd import grad


# Problem 1
def golden_section(f, a, b, tol=1e-5, maxiter=15):
    """Use the golden section search to minimize the unimodal function f.

    Parameters:
        f (function): A unimodal, scalar-valued function on [a,b].
        a (float): Left bound of the domain.
        b (float): Right bound of the domain.
        tol (float): The stopping tolerance.
        maxiter (int): The maximum number of iterations to compute.

    Returns:
        (float): The approximate minimizer of f.
        (bool): Whether or not the algorithm converged.
        (int): The number of iterations computed.
    """
    x0 = (a+b)/2                   #Set the initial minimizer approx as interval mid
    phi = (1+np.sqrt(5))/2
    for i in range(maxiter):  #Iterate only maxiter times at most
        c = (b-a)/phi
        atil = b-c
        btil = a+c
        if f(atil) <= f(btil):        #Get new boundaries for the search interval
            b = btil
        else:
            a = atil
        x1 = (a+b)/2               #Set the minimizer approx as the interval midpoint
        if np.abs(x0 - x1) < tol:
            break                  #Stop iterating if the approx stops changing enough
        x0 = x1
    return x1


# Problem 2
def newton1d(df, d2f, x0, tol=1e-5, maxiter=15):
    """Use Newton's method to minimize a function f:R->R.

    Parameters:
        df (function): The first derivative of f.
        d2f (function): The second derivative of f.
        x0 (float): An initial guess for the minimizer of f.
        tol (float): The stopping tolerance.
        maxiter (int): The maximum number of iterations to compute.

    Returns:
        (float): The approximate minimizer of f.
        (bool): Whether or not the algorithm converged.
        (int): The number of iterations computed.
    """

    for _ in range(maxiter):
        xtil = x0 - df(x0)/d2f(x0)
        if abs(xtil - x0) < tol:
            break
        x0 = xtil
    return xtil
    


# Problem 3
def secant1d(df, x0, x1, tol=1e-5, maxiter=15):
    """Use the secant method to minimize a function f:R->R.

    Parameters:
        df (function): The first derivative of f.
        x0 (float): An initial guess for the minimizer of f.
        x1 (float): Another guess for the minimizer of f.
        tol (float): The stopping tolerance.
        maxiter (int): The maximum number of iterations to compute.

    Returns:
        (float): The approximate minimizer of f.
        (bool): Whether or not the algorithm converged.
        (int): The number of iterations computed.
    """
    for _ in range(maxiter):
        xkprime = df(x1)
        xnext = x1 - (x1 - x0)*xkprime/(xkprime-df(x0))
        if abs(xnext - x1) < tol:
            break
        x0 = x1
        x1 = xnext
        
    return xnext
        
    

# Problem 4
def backtracking(f, Df, x, p, alpha=1, rho=.9, c=1e-4):
    """Implement the backtracking line search to find a step size that
    satisfies the Armijo condition.

    Parameters:
        f (function): A function f:R^n->R.
        Df (function): The first derivative (gradient) of f.
        x (float): The current approximation to the minimizer.
        p (float): The current search direction.
        alpha (float): A large initial step length.
        rho (float): Parameter in (0, 1).
        c (float): Parameter in (0, 1).

    Returns:
        alpha (float): Optimal step size.
    """
    
    Dfp = np.dot((Df(x).T),(p))    #Compute these values only once
    fx = f(x)
    while f(x + (alpha*p)) > (fx + c*alpha*Dfp):
        alpha = rho*alpha
    return alpha
    
