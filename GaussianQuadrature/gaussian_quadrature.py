# quassian_quadrature.py
"""Volume 2: Gaussian Quadrature.
<Name>
<Class>
<Date>
"""

import numpy as np
from scipy import linalg as la
from scipy.stats import norm
from matplotlib import pyplot as plt
from scipy.integrate import quad

class GaussianQuadrature:
    """Class for integrating functions on arbitrary intervals using Gaussian
    quadrature with the Legendre polynomials or the Chebyshev polynomials.
    """
    # Problems 1 and 2
    def __init__(self, n, polytype="legendre"):
        """Calculate and store the n points and weights corresponding to the
        specified class of orthogonal polynomial (Problem 3). Also store the
        inverse weight function w(x)^{-1} = 1 / w(x).

        Parameters:
            n (int): Number of points and weights to use in the quadrature.
            polytype (string): The class of orthogonal polynomials to use in
                the quadrature. Must be either 'legendre' or 'chebyshev'.

        Raises:
            ValueError: if polytype is not 'legendre' or 'chebyshev'.
        """
        if polytype != "legendre" and polytype != "chebyshev":
            raise ValueError("polytype is not 'legendre' or 'chebyshev'")
        
        self.polytype = polytype  #store polynomial type
        self.n = n                #number of points
        if polytype == 'legendre':
            self.winv = lambda x: 1  #store w^-1
        else:
            self.winv = lambda x: np.sqrt(1-x**2) #store w^-1
            
        self.points, self.weights = self.points_weights(self.n) #call points_weights

    # Problem 2
    def points_weights(self, n):
        """Calculate the n points and weights for Gaussian quadrature.

        Parameters:
            n (int): The number of desired points and weights.

        Returns:
            points ((n,) ndarray): The sampling points for the quadrature.
            weights ((n,) ndarray): The weights corresponding to the points.
        """
        if self.polytype == 'legendre':
            Beta = [k**2/(4*k**2-1) for k in range(1,n+1)] #Beta values
        if self.polytype == 'chebyshev':
            Beta = [1/2]+[1/4]*n                          #Beta Values
            
        J = np.zeros((n,n))  #Create J matrix
        for k in range(n):
            if k == 0:
                J[k][k+1] = np.sqrt(Beta[k])
            elif k == n-1:
                J[k][k-1] = np.sqrt(Beta[k-1])
            else:
                J[k][k+1] = np.sqrt(Beta[k])
                J[k][k-1] = np.sqrt(Beta[k-1])
        
        
        eigvals, eigvecs = la.eig(J)

        if self.polytype == 'legendre':
            mu = 2
        else:
            mu = np.pi
        
        w = [mu*((eigvecs[0][i])**2) for i in range(n)] #w = mu*eig_0,i^2

        return np.real(eigvals), w
        
            

    # Problem 3
    def basic(self, f):
        """Approximate the integral of a f on the interval [-1,1]."""
        g = lambda x: f(x) * self.winv(x)      #g(x) = f(x)*w^-1(x)
        integral = np.sum(g(self.points) * self.weights)  #sum(g(points).T*weights)
        return integral
        
        
    # Problem 4
    def integrate(self, f, a, b):
        """Approximate the integral of a function on the interval [a,b].

        Parameters:
            f (function): Callable function to integrate.
            a (float): Lower bound of integration.
            b (float): Upper bound of integration.

        Returns:
            (float): Approximate value of the integral.
        """
        h = lambda x: f((b-a)*x/2 + (a+b)/2)   #define h(x)
        g = lambda x: h(x) * self.winv(x)
        integral = np.sum(g(self.points) * self.weights)
        return (b-a)*integral/2

    # Problem 6.
    def integrate2d(self, f, a1, b1, a2, b2):
        """Approximate the integral of the two-dimensional function f on
        the interval [a1,b1]x[a2,b2].

        Parameters:
            f (function): A function to integrate that takes two parameters.
            a1 (float): Lower bound of integration in the x-dimension.
            b1 (float): Upper bound of integration in the x-dimension.
            a2 (float): Lower bound of integration in the y-dimension.
            b2 (float): Upper bound of integration in the y-dimension.

        Returns:
            (float): Approximate value of the integral.
        """
        h = lambda x, y : f((b1-a1)*x/2 + (a1+b1)/2, (b2-a2)*y/2 + (a2+b2)/2)
        g = lambda x, y : h(x,y)*((self.winv(x)*self.winv(y)))
        w = self.weights
        z = self.points
        
        integral = np.sum([w[i]*w[j]*g(z[i],z[j]) for j in range(self.n) for i in range(self.n)]) #nested summations
        return (b1-a1)*(b2-a2)*integral/4

# Problem 5
def prob5():
    """Use scipy.stats to calculate the "exact" value F of the integral of
    f(x) = (1/sqrt(2 pi))e^((-x^2)/2) from -3 to 2. Then repeat the following
    experiment for n = 5, 10, 15, ..., 50.
        1. Use the GaussianQuadrature class with the Legendre polynomials to
           approximate F using n points and weights. Calculate and record the
           error of the approximation.
        2. Use the GaussianQuadrature class with the Chebyshev polynomials to
           approximate F using n points and weights. Calculate and record the
           error of the approximation.
    Plot the errors against the number of points and weights n, using a log
    scale for the y-axis. Finally, plot a horizontal line showing the error of
    scipy.integrate.quad() (which doesn’t depend on n).
    """
    
    f = lambda x: np.exp((-x**2)/2)/np.sqrt(2*np.pi)

    exact = norm.cdf(2) - norm.cdf(-3)
    
    legerror = []  #legendre error
    cheberror = [] #If you read this I'll buy you a candy bar
    fives = np.arange(5,55,5)
    for n in fives:
        leg = GaussianQuadrature(n)
        legerror.append(abs(leg.integrate(f,-3,2)-exact))
        cheb = GaussianQuadrature(n, 'chebyshev')
        cheberror.append(abs(cheb.integrate(f,-3,2)-exact))
    
    plt.semilogy(fives, legerror, 'k', label = 'Legendre Error')
    plt.semilogy(fives, cheberror, 'r', label = 'Chebyshev Error')
    plt.semilogy(fives, [abs(quad(f,-3,2)-exact)[0]]*10, label = 'quad error')
    plt.legend()