# test_specs.py
"""Python Essentials: Unit Testing.
<Name>
<Class>
<Date>
"""

import specs
import pytest


def test_add():
    assert specs.add(1, 3) == 4, "failed on positive integers"
    assert specs.add(-5, -7) == -12, "failed on negative integers"
    assert specs.add(-6, 14) == 8

def test_divide():
    assert specs.divide(4,2) == 2, "integer division"
    assert specs.divide(5,4) == 1.25, "float division"
    with pytest.raises(ZeroDivisionError) as excinfo:
        specs.divide(4, 0)
    assert excinfo.value.args[0] == "second input cannot be zero"


# Problem 1: write a unit test for specs.smallest_factor(), then correct it.
def test_smallest_factor():
    assert specs.smallest_factor(25)==5
    assert specs.smallest_factor(10)==2
    assert specs.smallest_factor(435)==3
    assert specs.smallest_factor(29)==29
    assert specs.smallest_factor(73)==73
    assert specs.smallest_factor(1)==1
# Problem 2: write a unit test for specs.month_length().
def test_month_length():
    assert specs.month_length("September")==30
    assert specs.month_length("October")==31
    assert specs.month_length("February")==28
    assert specs.month_length("February",True)==29
    assert specs.month_length("Brodruary")==None



# Problem 3: write a unit test for specs.operate().
def test_operate():
    with pytest.raises(TypeError) as exinfo:
        specs.operate(1,2,2)
    assert exinfo.value.args[0]=="oper must be a string"
    with pytest.raises(ZeroDivisionError) as excinfo:
        specs.operate(1,0,"/")
    assert excinfo.value.args[0]=="division by zero is undefined"
    with pytest.raises(ValueError) as exbinfo:
        specs.operate(1,0,"fish")
    assert exbinfo.value.args[0]=="oper must be one of '+', '/', '-', or '*'"
    assert specs.operate(4,2,'+')==6, "addition fails"
    assert specs.operate(-9,1,'+')==-8
    assert specs.operate(-2, -2,'+')==-4
    assert specs.operate(4,-8,'+')==-4
    assert specs.operate(0,1,'+')==1
    assert specs.operate(0,0,'+')==0

    assert specs.operate(4,2,'*')==8
    assert specs.operate(-4,2,'*')==-8
    assert specs.operate(-4,-2,'*')==8
    assert specs.operate(0,1,'*')==0
    assert specs.operate(4,-2,'*')==-8
    
    assert specs.operate(4,2,'/')==2
    assert specs.operate(-4,2,'/')==-2
    assert specs.operate(-4,-2,'/')==2
    assert specs.operate(0,1,'/')==0
    
    assert specs.operate(4,2,'-')==2
    assert specs.operate(-4,2,'-')==-6
    assert specs.operate(-4,-2,'-')==-2
    assert specs.operate(0,1,'-')==-1

# Problem 4: write unit tests for specs.Fraction, then correct it.
@pytest.fixture
def set_up_fractions():
    frac_1_3 = specs.Fraction(1, 3)
    frac_1_2 = specs.Fraction(1, 2)
    frac_n2_3 = specs.Fraction(-2, 3)
    frac_3_1 = specs.Fraction(3, 1)
    return frac_1_3, frac_1_2, frac_n2_3, frac_3_1

# def test_fraction_truediv():
#     with pytest.raises(ZeroDivisionError) as exinfo:
        
#     assert exinfo.value.args[0]=="oper must be a string"


def test_fraction_init(set_up_fractions):
    frac_1_3, frac_1_2, frac_n2_3, frac_3_1 = set_up_fractions
    assert frac_1_3.numer == 1
    assert frac_1_2.denom == 2
    assert frac_n2_3.numer == -2
    frac = specs.Fraction(30, 42)
    assert frac.numer == 5
    assert frac.denom == 7
    with pytest.raises(ZeroDivisionError) as exinfo:
        specs.Fraction(1, 0)
    assert exinfo.value.args[0]=="denominator cannot be zero"
    with pytest.raises(TypeError) as exinfo:
        specs.Fraction('fish', 'eater')
    assert exinfo.value.args[0]=="numerator and denominator must be integers"

def test_fraction_str(set_up_fractions):
    frac_1_3, frac_1_2, frac_n2_3, frac_3_1 = set_up_fractions
    assert str(frac_1_3) == "1/3"
    assert str(frac_1_2) == "1/2"
    assert str(frac_n2_3) == "-2/3"
    assert str(frac_3_1) == "3"

def test_fraction_float(set_up_fractions):
    frac_1_3, frac_1_2, frac_n2_3, frac_3_1 = set_up_fractions
    assert float(frac_1_3) == 1 / 3.
    assert float(frac_1_2) == .5
    assert float(frac_n2_3) == -2 / 3.
    assert float(frac_3_1) == 3

def test_fraction_eq(set_up_fractions):
    frac_1_3, frac_1_2, frac_n2_3, frac_3_1 = set_up_fractions
    assert frac_1_2 == specs.Fraction(1, 2)
    assert frac_1_3 == specs.Fraction(2, 6)
    assert frac_n2_3 == specs.Fraction(8, -12)
    assert frac_3_1 == 3
    
def test_fraction_add(set_up_fractions):
    frac_1_3, frac_1_2, frac_n2_3, frac_3_1 = set_up_fractions
    assert frac_1_2 + frac_1_3 == specs.Fraction(5,6)
    assert frac_1_3 + frac_n2_3 == specs.Fraction(-1,3)
    
def test_fraction_subtract(set_up_fractions):
    frac_1_3, frac_1_2, frac_n2_3, frac_3_1 = set_up_fractions
    assert frac_1_2-frac_1_3 == specs.Fraction(1, 6)
    assert frac_1_2-frac_n2_3 == specs.Fraction(7, 6)
    
def test_fraction_mul(set_up_fractions):
    frac_1_3, frac_1_2, frac_n2_3, frac_3_1 = set_up_fractions
    assert frac_1_2*frac_1_3 == specs.Fraction(1, 6)
    assert frac_1_3 * frac_n2_3 == specs.Fraction(-2, 9)
    assert frac_1_2 * specs.Fraction(1,1) == specs.Fraction(1,2)
    assert frac_1_2 * specs.Fraction(0, 1) == specs.Fraction(0,1)
    
def test_fraction_truediv(set_up_fractions):
    frac_1_3, frac_1_2, frac_n2_3, frac_3_1 = set_up_fractions
    with pytest.raises(ZeroDivisionError) as exinfo:
        frac_1_2/specs.Fraction(0, 1)
    assert exinfo.value.args[0]=="cannot divide by zero"
    assert frac_1_2/frac_1_3 == specs.Fraction(3, 2)
    
# Problem 5: Write test cases for Set.



"""Good hand"""
hand_1 =  ["1022", "1122", "0100", "2021",
           "0010", "2201", "2111", "0020",
           "1102", "0210", "2110", "1020"]
   
"""not enough digits"""
hand_2 = ["10", "1122", "0100", "2021",
           "0010", "2201", "2111", "0020",
           "1102", "0210", "2110", "0020"]

"""repeat card"""
hand_3 =  ["1022", "1122", "0100", "2021",
           "0010", "2201", "2111", "0020",
           "1102", "0210", "2110", "0020"]

"""Too few"""
hand_4 = ["1022", "1122", "0100", "2021",
          "0010", "2201", "2111", "0020",
          "0210", "2110", "1020"]

"""Too many"""
hand_5 =  ["1022", "1122", "0100", "2021",
           "0010", "2201", "2111", "0020",
           "1102", "0210", "2110", "1020","1111"]

"""digit != 0,1,2"""
hand_6 =  ["1022", "1122", "0100", "2021",
           "0010", "2201", "2111", "0020",
           "1302", "0210", "2110", "1020"]

def test_count_sets():
    with pytest.raises(ValueError) as excinfo:
        specs.count_sets(hand_2)
    assert excinfo.value.args[0] == "one or more cards does not have exactly 4 digits"
    with pytest.raises(ValueError) as excinfo:
        specs.count_sets(hand_3)
    assert excinfo.value.args[0] == "the cards are not all unique"
    with pytest.raises(ValueError) as excinfo:
        specs.count_sets(hand_4)
    assert excinfo.value.args[0] == "there are not exactly 12 cards"
    with pytest.raises(ValueError) as excinfo:
        specs.count_sets(hand_5)
    assert excinfo.value.args[0] == "there are not exactly 12 cards"
    with pytest.raises(ValueError) as excinfo:
        specs.count_sets(hand_6)
    assert excinfo.value.args[0] == "one or more cards has a character other than 0, 1, or 2"
    assert specs.count_sets(hand_1) == 6
    
def test_is_set():

    a1=hand_1[0]
    a2=hand_1[3]
    a3=hand_1[7]
    b1=hand_1[0]
    b2=hand_1[1]
    b3=hand_1[2]
    assert specs.is_set(a1,a2,a3) == True
    assert specs.is_set(b1,b2,b3) == False

# print(specs.count_sets(hand_1))
    