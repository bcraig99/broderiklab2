# dynamic_programming.py
"""Volume 2: Dynamic Programming.
<Name>
<Class>
<Date>
"""

import numpy as np
from matplotlib import pyplot as plt

def calc_stopping(N):
    """Calculate the optimal stopping time and expected value for the
    marriage problem.

    Parameters:
        N (int): The number of candidates.

    Returns:
        (float): The maximum expected value of choosing the best candidate.
        (int): The index of the maximum expected value.
    """
     
        
    V = {}
    V[N] = 0
    for i in range(N-1,0,-1):
        V[i] = max((i)*V[i+1]/(i+1)+1/N, V[i+1])#Will take the max of a manipulated
                                                #and the previous iteration
        if V[i] == V[i+1]:
            return V[i+1], i+1
    return V[1], 1

# Problem 2
def graph_stopping_times(M):
    """Graph the optimal stopping percentage of candidates to interview and
    the maximum probability against M.

    Parameters:
        M (int): The maximum number of candidates.

    Returns:
        (float): The optimal stopping percent of candidates for M.
    """
    
    opt_per = []
    thing = []
    for m in range(3,M+1):
        opt_per.append(calc_stopping(m)[1]/m)
        thing.append(calc_stopping(m)[0])
        
    f = plt.figure(figsize = (10,10))               #graphs the optimal stopping percentage
    ax1 = f.add_subplot(211)
    ax1.plot(np.arange(3,M+1),opt_per, color = 'k')
    plt.title('Optimal Stopping Percentage')
    
    ax2 = f.add_subplot(212)                        #graphs maximum probability
    ax2.plot(np.arange(3,M+1), thing, color = 'k')
    plt.title('Maximum Probability')
    
    plt.tight_layout()
    plt.show()
    
    return(calc_stopping(M)[1]/M)

# Problem 3
def get_consumption(N, u=lambda x: np.sqrt(x)):
    """Create the consumption matrix for the given parameters.

    Parameters:
        N (int): Number of pieces given, where each piece of cake is the
            same size.
        u (function): Utility function.

    Returns:
        C ((N+1,N+1) ndarray): The consumption matrix.
    """
    C = np.zeros((N+1,N+1))
    for i in range(N+1):
        for j in range(i):
            C[i][j] = u(i/N - j/N) #possible cake to be eaten
            
    return C


# Problems 4-6
def eat_cake(T, N, B, u=lambda x: np.sqrt(x)):
    """Create the value and policy matrices for the given parameters.

    Parameters:
        T (int): Time at which to end (T+1 intervals).
        N (int): Number of pieces given, where each piece of cake is the
            same size.
        B (float): Discount factor, where 0 < B < 1.
        u (function): Utility function.

    Returns:
        A ((N+1,T+1) ndarray): The matrix where the (ij)th entry is the
            value of having w_i cake at time j.
        P ((N+1,T+1) ndarray): The matrix where the (ij)th entry is the
            number of pieces to consume given i pieces at time j.
    """
    w = [i/N for i in range(N+1)]
    A = np.zeros((N+1, T+1))
    P = np.zeros((N+1, T+1))
    for i in range(N+1):
        A[i][T] = u(i/N)
        P[i][T] = i/N
    for t in range(T,0,-1):
        CV = np.zeros((N+1,N+1))
        for i in range(N+1):
            for j in range(0,i):
                CV[i][j] = u(i/N - j/N) + B*A[j][t] #We're going to work backward
                                                    #to build CV matrices
        
        for i in range(N+1):
            A[i][t-1] = max(CV[i][:])               #A matrix is rows of CV
            P[i][t-1] = w[i]-w[np.argmax(CV[i][:])] #P matrix is w[i] minus the
                                                    #index of largest value
    
    return A, P


# Problem 7
def find_policy(T, N, B, u=np.sqrt):
    """Find the most optimal route to take assuming that we start with all of
    the pieces. Show a graph of the optimal policy using graph_policy().

    Parameters:
        T (int): Time at which to end (T+1 intervals).
        N (int): Number of pieces given, where each piece of cake is the
            same size.
        B (float): Discount factor, where 0 < B < 1.
        u (function): Utility function.

    Returns:
        ((T,) ndarray): The matrix describing the optimal percentage to
            consume at each time.
    """
    A,P = eat_cake(T,N,B)
    c = []
    n = N
    for t in range(T+1):
        c.append(P[n,t])
        n -= int(c[-1]*N +.5)
    return c