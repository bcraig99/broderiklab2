# gradient_methods.py
"""Volume 2: Gradient Descent Methods.
<Name>
<Class>
<Date>
"""
import numpy as np
from scipy import optimize as opt
from autograd import grad
from autograd import jacobian
from scipy import linalg as la
import scipy
from matplotlib import pyplot as plt

# Problem 1
def steepest_descent(f, Df, x0, tol=1e-5, maxiter=100):
    """Compute the minimizer of f using the exact method of steepest descent.

    Parameters:
        f (function): The objective function. Accepts a NumPy array of shape
            (n,) and returns a float.
        Df (function): The first derivative of f. Accepts and returns a NumPy
            array of shape (n,).
        x0 ((n,) ndarray): The initial guess.
        tol (float): The stopping tolerance.
        maxiter (int): The maximum number of iterations to compute.

    Returns:
        ((n,) ndarray): The approximate minimum of f.
        (bool): Whether or not the algorithm converged.
        (int): The number of iterations computed.
    """
    k = 1
    boo = False
    while k < maxiter:
        alpha = opt.minimize_scalar(lambda alph: f(x0-alph*Df(x0).T))['x'] #Optimize alpha
        xnext = x0 - alpha*Df(x0).T #Gradient Descent
        if np.linalg.norm(Df(x0), ord = np.inf) < tol: 
            boo = True
            break
        x0 = xnext
        k += 1
    
    return f(x0), boo, k

# Problem 2
def conjugate_gradient(Q, b, x0, tol=1e-4):
    """Solve the linear system Qx = b with the conjugate gradient algorithm.

    Parameters:
        Q ((n,n) ndarray): A positive-definite square matrix.
        b ((n, ) ndarray): The right-hand side of the linear system.
        x0 ((n,) ndarray): An initial guess for the solution to Qx = b.
        tol (float): The convergence tolerance.

    Returns:
        ((n,) ndarray): The solution to the linear system Qx = b.
        (bool): Whether or not the algorithm converged.
        (int): The number of iterations computed.
    """
    n = Q.shape[0]
    r = Q@x0 - b
    d= -r
    k = 0
    while la.norm(r, ord = 2) >= tol or k < n:
        ak = (r.T@r)/(d.T@Q@d)
        xnext = x0 + (ak*d)
        rnext = r + (ak*Q@d)
        Beta = (rnext.T@rnext)/(r.T@r)
        dnext = -rnext + Beta*d
        k += 1
        r = rnext
        x0 = xnext
        d = dnext
    return xnext

# Problem 3
def nonlinear_conjugate_gradient(f, Df, x0, tol=1e-5, maxiter=100):
    """Compute the minimizer of f using the nonlinear conjugate gradient
    algorithm.

    Parameters:
        f (function): The objective function. Accepts a NumPy array of shape
            (n,) and returns a float.
        Df (function): The first derivative of f. Accepts and returns a NumPy
            array of shape (n,).
        x0 ((n,) ndarray): The initial guess.
        tol (float): The stopping tolerance.
        maxiter (int): The maximum number of iterations to compute.

    Returns:
        ((n,) ndarray): The approximate minimum of f.
        (bool): Whether or not the algorithm converged.
        (int): The number of iterations computed.
    """
    r0 = -Df(x0).T
    d0 = r0
    a0 = opt.minimize_scalar(lambda alph: f(x0+alph*d0))['x']
    x1 = x0 + a0*d0
    k = 1
    while la.norm(r0) >= tol:
        if k%10 == 0:
            print(k)
        r1 = -Df(x1).T
        Beta = (r1.T@r1)/(r0.T@r0)
        d1 = r1 + (Beta*d0)
        ak = opt.minimize_scalar(lambda alph: f(x1+alph*d1))['x']
        x2 = x1 + (ak*d1)
        k += 1
        r0 = r1
        d0 = d1
        x1 = x2
        if k >= maxiter:
            break
    return x1


# Problem 4
def prob4(filename="linregression.txt",
          x0=np.array([-3482258, 15, 0, -2, -1, 0, 1829])):
    """Use conjugate_gradient() to solve the linear regression problem with
    the data from the given file, the given initial guess, and the default
    tolerance. Return the solution to the corresponding Normal Equations.
    """

    data = np.loadtxt(filename)
    b = data[:,0].copy()
    data[:,0] = np.ones(b.shape)

    return conjugate_gradient(data.T@data, data.T@b, x0, tol=1e-4)
    


# Problem 5
class LogisticRegression1D:
    """Binary logistic regression classifier for one-dimensional data."""

    def fit(self, x, y, guess):
        """Choose the optimal beta values by minimizing the negative log
        likelihood function, given data and outcome labels.

        Parameters:
            x ((n,) ndarray): An array of n predictor variables.
            y ((n,) ndarray): An array of n outcome variables.
            guess (array): Initial guess for beta.
        """
        
        neglog = lambda beta: np.sum([np.log(1+np.exp(-beta[0]-beta[1]*x[i])) + (1-y[i])*(beta[0]+beta[1]*x[i]) for i in range(len(x))])
        self.Beta = opt.fmin_cg(neglog, guess)
    
    def predict(self, x):
        """Calculate the probability of an unlabeled predictor variable
        having an outcome of 1.

        Parameters:
            x (float): a predictor variable with an unknown label.
        """
        return 1/(1 + np.exp(-self.Beta[0]-self.Beta[1]*x))


# Problem 6
def prob6(filename="challenger.npy", guess=np.array([20., -1.])):
    """Return the probability of O-ring damage at 31 degrees Farenheit.
    Additionally, plot the logistic curve through the challenger data
    on the interval [30, 100].

    Parameters:
        filename (str): The file to perform logistic regression on.
                        Defaults to "challenger.npy"
        guess (array): The initial guess for beta.
                        Defaults to [20., -1.]
    """
    data = np.load(filename)
    x = data[:,0]
    y = data[:,1]
    shuttle = LogisticRegression1D()
    shuttle.fit(x,y,guess)
    X = np.linspace(30,100,200)

    
    plt.plot(X,shuttle.predict(X), color = 'k')
    plt.scatter(x,y, marker = '+', label = 'Previous Damage Binary')
    plt.scatter(31, shuttle.predict(31), color = 'orange', label = 'P(Damage) at Launch')
    plt.legend()
    plt.xlabel('Temperature')
    plt.ylabel('O-Ring Damage')
    plt.title("Probability of O-Ring Damamge")
    
    
    
    
if __name__ == '__main__':
    pass
