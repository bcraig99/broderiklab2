# python_intro.py
"""Python Essentials: Introduction to Python.
Broderik S. Craig
MATH 321 Section 3
<Date>
"""
import numpy as np

#Problem 1
def isolate(a, b, c, d, e):
    """This will take a given 5-element list and return it with 5 spaces between the first 3 elements and 1 space between the rest."""
    print(a,b,c,sep='     ',end=' '), print(d,e) #The first print statement has sep set to 5 spaces, overrides the automatic \n command, and leaves a space before the next print statment

#Problem 2
def first_half(string):
    """This function takes a given string, finds its length, then splices the string at its halfway point"""
    length=len(string)        #The length of the string
    return string[:length//2] #returns the string starting at index 0 and going to the index at half the string's length. Integer division is used for when the string has an odd length


def backward(first_string):
    """This function inputs a string and returns it in reverse order. It gives the mirror version"""
    new_string='' #Create a new empty string
    for i in range(len(first_string)):  #iterate as many times as there are characters in the input string
        new_string+=first_string[-i-1] #add each character of the first string to the new string starting at the last character
    return new_string

#Problem 3
def list_ops():
    animals=['bear','ant','cat','dog']
    # print(0,animals)
    animals.append('eagle')
    # print(1,animals)
    animals.remove(animals[2])
    animals.insert(2,'fox')
    # print(2,animals)
    animals.remove(animals[1])
    # print(3,animals)
    animals.sort(reverse=True)
    # print(4,animals)
    index = animals.index('eagle')
    animals.remove('eagle')
    animals.insert(index,'hawk')
    # print(5,animals)
    animals[-1]=animals[-1]+'hunter'
    
    return animals

#Problem 4
    """Returns the partial sum of the first n terms of the alternating
    harmonic series. Used to approximate ln(2).
    """
def alt_harmonic(n):
    
    harmsum = sum([((-1)**(i+1))/i for i in range(1,n+1)])
    
    return harmsum
    
    
    

def prob5(A):
    """Returns an array with negative numbers replaced with 0"""
    A_copy=np.array(A)  #create a copy of the array
    A_copy[A_copy<0]=0  #the format array[condition] will identify all elements of the array that meet that condition
    return A_copy

def prob6():
    """Stacks the given matrices as described in the book"""
    A=np.array([0,2,4,1,3,5]).reshape(2,3)   #Define A as an array and reshape it to a 2x3 matrix
    B=np.tril(3*np.ones(3))                  #sets B as a matrix with its lower triangle filled with 3's
    C=-2*np.eye(3,3)                         #eye is the identity matrix
    I=np.eye(3,3)
    zero22=np.array([0,0,0,0]).reshape(2,2)
    zero32=np.full_like(A.T,0)              #creates a matrix of zeros of the same shape of A.T
    zero33=np.full_like(C,0)
    zero23=np.full_like(A,0)
    
    col1=np.vstack((zero33,A,B))             #column with each matrix stacked on top of eachother
    col2=np.vstack((A.T,zero22,zero32))
    col3=np.vstack((I,zero23,C))
    
    blockmatrix=np.hstack((col1,col2,col3))  #set the columns next to eachother
    
    return blockmatrix
    
    
def prob7(A):
    rowsum=A.sum(axis=1,keepdims=True) #axis 1 is columnwise, axis 0 is rowwise. keepdims tells the function to keep the result as a column
    A=A/rowsum                         #
    return A

def prob8():
    grid = np.load('grid.npy')
    # grid[ROWbegin:ROWend:ROWstep,COLUMNbegin:COLUMNend:COLUMNstep])
    horizontal=np.max(grid[:,:-3] * grid[:,1:-2] * grid[:,2:-1] * grid[:,3:])  #must be of equal length. iterates through each index as defined
    vertical=np.max(grid[:-3,:] * grid[1:-2,:] * grid[2:-1,:] * grid[3:,:])    #
    rightdiag=np.max(grid[:-3,:-3] * grid[1:-2,1:-2] * grid[2:-1,2:-1] * grid[3:,3:])
    leftdiag=np.max(grid[3:,:-3] * grid[2:-1,1:-2] * grid[1:-2,2:-1] * grid[:-3,3:])    
    
    return max(horizontal,vertical,rightdiag,leftdiag)
    
    
    
    
    
    
    
    
    
    
