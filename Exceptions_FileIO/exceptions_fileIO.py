# exceptions_fileIO.py
"""Python Essentials: Exceptions and File Input/Output.
<Name>
<Class>
<Date>
"""

from random import choice
import numpy as np

# Problem 1
def arithmagic():
    """
    Takes in user input to perform a magic trick and prints the result.
    Verifies the user's input at each step and raises a
    ValueError with an informative error message if any of the following occur:
    
    The first number step_1 is not a 3-digit number.
    The first number's first and last digits differ by less than $2$.
    The second number step_2 is not the reverse of the first number.
    The third number step_3 is not the positive difference of the first two numbers.
    The fourth number step_4 is not the reverse of the third number.
    """
    
    """Step 1"""
    step_1 = input('Choose a 3-digit number where the first and last digits differ by 2 or more')
    if abs(int(step_1[0])-int(step_1[-1]))<=1:
        raise ValueError("The first number's first and last digits differ by less than $2$.")
    if len(step_1)!=3:
        raise ValueError("Integer longer than 3 digits.")

    
    """Step 2"""

    step_2 = input("Enter the reverse of the first number, obtained "
                                          "by reading it backwards: ")
    step2r=step_2[::-1]     #Reverse of step2
    if step_1!=step2r:
        raise ValueError("The second number step_2 is not the reverse of the first number.")

        
    """Step 3"""
    step_3 = input("Enter the positive difference of these numbers: ")
    if int(step_3)!=abs(int(step_1)-int(step_2)):
        raise ValueError("The third number step_3 is not the positive difference of the first two numbers")
        
    
    """Step 4"""
    
    step_4 = input("Enter the reverse of the previous result: ")
    step3r=step_3[::-1]  #The reverse of step 3
    if step3r!=step_4:
        raise ValueError("The fourth number step_4 is not the reverse of the third number.")
    print(str(step_3), "+", str(step_4), "= 1089 (ta-da!)")


# Problem 2
def random_walk(max_iters=1e12):
    """
    If the user raises a KeyboardInterrupt by pressing ctrl+c while the 
    program is running, the function should catch the exception and 
    print "Process interrupted at iteration $i$".
    If no KeyboardInterrupt is raised, print "Process completed".

    Return walk.
    """
    try:
        walk = 0
        directions = [1, -1]
        for i in range(int(max_iters)):
            walk += choice(directions)
        print("Process completed")
        return walk
    except:
        print("Process interrupted at iteration", i)        
        return walk
# Problems 3 and 4: Write a 'ContentFilter' class.
    """Class for reading in file
        
    Attributes:
        filename (str): The name of the file
        contents (str): the contents of the file
        
    """
class ContentFilter(object):   
    # Problem 3
    def __init__(self, filename):
        """Read from the specified file. If the filename is invalid, prompt
        the user until a valid filename is given.
        """
        while True:
            try:
                with open(filename) as file:
                    self.contents=file.read()
                    self.name=file.name
                break  
            except (FileNotFoundError, FileExistsError, IOError, SyntaxError):
                filename=input("Please input a valid filename:")

        
        
        
 # Problem 4 ---------------------------------------------------------------
    def check_mode(self, mode):
        """Raise a ValueError if the mode is invalid."""
        if mode != 'w' and mode !='x' and  mode != 'a':
            raise ValueError("Please input w, x, or a as your mode.")
        

    def uniform(self, outfile, mode='w', case='upper'):
        """Write the data ot the outfile in uniform case."""
        with open(outfile,mode) as outfile:
            if case=='upper':
                outfile.write(self.contents.upper())
            elif case=='lower':
                outfile.write(self.contents.lower())
            else:
                raise ValueError("Input case as 'upper' or 'lower'")
            

    def reverse(self, outfile, mode='w', unit='line'):
        """Write the data to the outfile in reverse order."""
        with open(outfile,mode) as f:
            lines = self.contents.strip('\n').split('\n')
            
            if unit == 'line':
                for i in range(1,len(lines)+1):
                    f.writelines(lines[-i]+'\n')
                   
            elif unit == 'word':
                for i in range(len(lines)):
                    words=lines[i].split(" ")
                    wordsreverse=[words[-k] for k in range(1,len(words)+1)]
                    for j in range(len(wordsreverse)):
                        f.write(wordsreverse[j]+" ")
                    f.write('\n')
            else:
                raise ValueError("Please input 'word' or 'line' as the unit")
            


    def transpose(self, outfile, mode='w'):
        """Write the transposed version of the data to the outfile."""
        with open(outfile,mode) as f:
            lines = self.contents.strip('\n').split('\n')
            matrix=[]
            for i in range(len(lines)):
                words=lines[i].split(" ")
                matrix.append(words)
            matrix2=np.array(matrix)
            matrix3=matrix2.T
            for i in range(len(matrix3)):
                for j in range(len(matrix3[i])):
                    f.write(matrix3[i][j]+' ')
                f.write('\n')
            
            
    def __str__(self):
        """String representation: info about the contents of the file."""
        lines = self.contents.strip('\n').split('\n')
        sumalpha=sum([s.isalpha() for s in self.contents])
        sumspace=sum([s.isspace() for s in self.contents])
        sumdigit=sum([s.isdigit() for s in self.contents])
        return str('Source file:'+'\t\t'+ str(self.name)+'\n'+
                   'Total characters:'+'\t'+str(sumalpha+sumspace+sumdigit)+'\n'+
                   'Alphabetic characters:' + '\t'+str(sumalpha)+'\n'+
                   'Numerical characters:' + '\t'+str(sumdigit)+'\n'+
                   'Whitespace characters:' + '\t'+str(sumspace)+'\n'+
                   'Number of lines:'+'\t'+str(len(lines)))