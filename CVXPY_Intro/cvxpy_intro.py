# cvxpy_intro.py
"""Volume 2: Intro to CVXPY.
<Name>
<Class>
<Date>
"""

import cvxpy as cp
import numpy as np

def prob1():
    """Solve the following convex optimization problem:

    minimize        2x + y + 3z
    subject to      x  + 2y         <= 3
                         y   - 4z   <= 1
                    2x + 10y + 3z   >= 12
                    x               >= 0
                          y         >= 0
                                z   >= 0

    Returns (in order):
        The optimizer x (ndarray)
        The optimal value (float)
    """
    
    x = cp.Variable(3, nonneg = True)
    c = np.array([2, 1, 3])
    objective = cp.Minimize(c.T @ x)
    
    A = np.array([1,2,0])
    B = np.array([0,1,-4])
    C = np.array([2,10,3])
    D = np.eye(3)
    constraints = [A @ x <= 3, B @ x <= 1, C @ x >= 12, D @ x >= 0]
    
    problem = cp.Problem(objective, constraints)
    problem.solve()
    
    return x.value, problem.solve()


# Problem 2
def l1Min(A, b):
    """Calculate the solution to the optimization problem

        minimize    ||x||_1
        subject to  Ax = b

    Parameters:
        A ((m,n) ndarray)
        b ((m, ) ndarray)

    Returns:
        The optimizer x (ndarray)
        The optimal value (float)
    """

    m,n = A.shape
    x = cp.Variable(n)    
    objective = cp.Minimize(cp.norm(x, 1))
    constraints = [A@x == b]
    problem = cp.Problem(objective, constraints)
    opt_value = problem.solve()
    return x.value, opt_value

# Problem 3
def prob3():
    """Solve the transportation problem by converting the last equality constraint
    into inequality constraints.

    Returns (in order):
        The optimizer x (ndarray)
        The optimal value (float)
    """
    x = cp.Variable(6, nonneg=True)
    c = np.array([4,7,6,8,8,9])
    objective = cp.Minimize(x @ c)
    A = np.array([1,1,0,0,0,0, 0,0,1,1,0,0, 0,0,0,0,1,1, 1,0,1,0,1,0,
                  0,1,0,1,0,1, 1,1,1,1,1,1]).reshape(6,6)
    n = np.array([7,2,4,5,8,13])
    constraints = [A@x == n]
    problem = cp.Problem(objective, constraints)
    opt_value = problem.solve()
    return x.value, opt_value
    

# Problem 4
def prob4():
    """Find the minimizer and minimum of

    g(x,y,z) = (3/2)x^2 + 2xy + xz + 2y^2 + 2yz + (3/2)z^2 + 3x + z

    Returns (in order):
        The optimizer x (ndarray)
        The optimal value (float)
    """
    q = np.array([3,2,1,2,4,2,1,2,3]).reshape(3,3) #What you said in Slack
    r = np.array([3,0,1])                          #was very disappointing
    x = cp.Variable(3)                             #you're not on my side
    prob = cp.Problem(cp.Minimize(cp.quad_form(x, q)/2 +r.T @ x)) #A haiku
    opt = prob.solve()
    return x.value, opt

# Problem 5
def prob5(A, b):
    """Calculate the solution to the optimization problem
        minimize    ||Ax - b||_2
        subject to  ||x||_1 == 1
                    x >= 0
    Parameters:
        A ((m,n), ndarray)
        b ((m,), ndarray)
        
    Returns (in order):
        The optimizer x (ndarray)
        The optimal value (float)
    """
    m, n = A.shape
    x = cp.Variable(n, nonneg=True)
    objective = cp.Minimize(cp.norm(A@x - b, 2))
    constraints = [x @ np.ones(n) == 1]
    prob = cp.Problem(objective, constraints)
    opt = prob.solve()
    return x.value, opt

# Problem 6
def prob6():
    """Solve the college student food problem. Read the data in the file 
    food.npy to create a convex optimization problem. The first column is 
    the price, second is the number of servings, and the rest contain
    nutritional information. Use cvxpy to find the minimizer and primal 
    objective.
    
    Returns (in order):
        The optimizer x (ndarray)
        The optimal value (float)
    """	 
    food = np.load('food.npy', allow_pickle = True)
    x = cp.Variable(18, nonneg=True)
    p = food[:,0]
    s = food[:,1]
    c = (food[:,2] * s.T) @ x
    f = (food[:,3] * s.T) @ x
    shat = (food[:,4] * s.T) @ x
    chat = (food[:,5] * s.T) @ x
    fhat = (food[:,6] * s.T) @ x
    phat = (food[:,7] * s.T) @ x
    objective = cp.Minimize(p@x)
    constraints = [c <= 2000, f <= 65, shat <= 50, chat >= 1000, fhat >= 25, phat >= 46]
    prob = cp.Problem(objective, constraints)
    opt = prob.solve()
    return x.value, opt  #eat potatoes, milk, and Ramen

if __name__ == '__main__':
    x = cp.Variable()