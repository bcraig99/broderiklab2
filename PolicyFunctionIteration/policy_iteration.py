# policy_iteration.py
"""Volume 2: Policy Function Iteration.
<Name>
<Class>
<Date>
"""

# Intialize P for test example
#Left =0
#Down = 1
#Right = 2
#Up= 3
import numpy as np
import gym
from gym import wrappers

P = {s : {a: [] for a in range(4)} for s in range(4)}
P[0][0] = [(0, 0, 0, False)]
P[0][1] = [(1, 2, -1, False)]
P[0][2] = [(1, 1, 0, False)]
P[0][3] = [(0, 0, 0, False)]
P[1][0] = [(1, 0, -1, False)]
P[1][1] = [(1, 3, 1, True)]
P[1][2] = [(0, 0, 0, False)]
P[1][3] = [(0, 0, 0, False)]
P[2][0] = [(0, 0, 0, False)]
P[2][1] = [(0, 0, 0, False)]
P[2][2] = [(1, 3, 1, True)]
P[2][3] = [(1, 0, 0, False)]
P[3][0] = [(0, 0, 0, True)]
P[3][1] = [(0, 0, 0, True)]
P[3][2] = [(0, 0, 0, True)]
P[3][3] = [(0, 0, 0, True)]


# Problem 1
def value_iteration(P, nS ,nA, beta = 1, tol=1e-8, maxiter=3000):
    """Perform Value Iteration according to the Bellman optimality principle.

    Parameters:
        P (dict): The Markov relationship
                (P[state][action] = [(prob, nextstate, reward, is_terminal)...]).
        nS (int): The number of states.
        nA (int): The number of actions.
        beta (float): The discount rate (between 0 and 1).
        tol (float): The stopping criteria for the value iteration.
        maxiter (int): The maximum number of iterations.

    Returns:
       v (ndarray): The discrete values for the true value function.
       n (int): number of iterations
    """
    
    V_old = [0]*nS
    for k in range(maxiter):
        V_new = V_old.copy()
        for s in range(nS):
            sa_vector = np.zeros(nA)
            for a in range(nA):
                for tuple_info in P[s][a]:
                    # tuple_info is a tuple of (probability, next state, reward, done)
                    p, s_, u, _ = tuple_info
                    # sums up the possible end states and rewards with given action
                    sa_vector[a] += (p * (u + beta * V_old[s_]))
                    #add the max value to the value function
            V_new[s] = np.max(sa_vector)
        if np.linalg.norm(np.array(V_old) - np.array(V_new)) < tol:
            break
        V_old = V_new.copy()
    return np.array(V_new), k
    
# Problem 2
def extract_policy(P, nS, nA, v, beta = 1.0):
    """Returns the optimal policy vector for value function v

    Parameters:
        P (dict): The Markov relationship
                (P[state][action] = [(prob, nextstate, reward, is_terminal)...]).
        nS (int): The number of states.
        nA (int): The number of actions.
        v (ndarray): The value function values.
        beta (float): The discount rate (between 0 and 1).

    Returns:
        policy (ndarray): which direction to move in from each square.
    """
    

    pival = np.zeros(nS)
    for s in range(nS):
        sa_vector = np.zeros(nA)
        for a in range(nA):
            for tuple_info in P[s][a]:
                p, s_, u, _ = tuple_info
                # print(v)
                sa_vector[a] += (p * (u + beta * v[s_]))
        pival[s] = np.argmax(sa_vector)
    return pival
    

# Problem 3
def compute_policy_v(P, nS, nA, policy, beta=1.0, tol=1e-8):
    """Computes the value function for a policy using policy evaluation.

    Parameters:
        P (dict): The Markov relationship
                (P[state][action] = [(prob, nextstate, reward, is_terminal)...]).
        nS (int): The number of states.
        nA (int): The number of actions.
        policy (ndarray): The policy to estimate the value function.
        beta (float): The discount rate (between 0 and 1).
        tol (float): The stopping criteria for the value iteration.

    Returns:
        v (ndarray): The discrete values for the true value function.
    """
    
    V_old = np.zeros(nS)
    for _ in range(5000):
        V_new = np.zeros(nS)
        for s in range(nS):
            for tuple_info in P[s][policy[s]]:
                p, s_, u, _ = tuple_info
                V_new[s] += (p * (u + beta * V_old[s_]))
        if np.linalg.norm(V_old-V_new) < tol:
            break
        V_old = V_new.copy()
    return np.array(V_new)
    

# Problem 4
def policy_iteration(P, nS, nA, beta=1, tol=1e-8, maxiter=200):
    """Perform Policy Iteration according to the Bellman optimality principle.

    Parameters:
        P (dict): The Markov relationship
                (P[state][action] = [(prob, nextstate, reward, is_terminal)...]).
        nS (int): The number of states.
        nA (int): The number of actions.
        beta (float): The discount rate (between 0 and 1).
        tol (float): The stopping criteria for the value iteration.
        maxiter (int): The maximum number of iterations.

    Returns:
    	v (ndarray): The discrete values for the true value function
        policy (ndarray): which direction to move in each square.
        n (int): number of iterations
    """
    v =value_iteration(P, nS ,nA, beta = 1, tol=1e-8, maxiter=3000)
    pi = extract_policy(P, nS, nA, v[0], beta = 1.0)
    for k in range(maxiter):
        vnext = compute_policy_v(P,nS,nA,pi)
        pinext = extract_policy(P, nS, nA, vnext)
        if np.linalg.norm(pinext - pi) < tol:
            break
        pi = pinext.copy()
    return vnext, pinext, k
        
# Problem 5 and 6
def frozen_lake(basic_case=True, M=1000, render=False):
    """ Finds the optimal policy to solve the FrozenLake problem

    Parameters:
    basic_case (boolean): True for 4x4 and False for 8x8 environemtns.
    M (int): The number of times to run the simulation using problem 6.
    render (boolean): Whether to draw the environment.
    
    Returns:
    vi_policy (ndarray): The optimal policy for value iteration.
    vi_total_rewards (float): The mean expected value for following the value iteration optimal policy.
    pi_value_func (ndarray): The maximum value function for the optimal policy from policy iteration.
    pi_policy (ndarray): The optimal policy for policy iteration.
    pi_total_rewards (float): The mean expected value for following the policy iteration optimal policy.
    """
    
    if basic_case == True:
        env_name = 'FrozenLake-v1'
        env = gym.make(env_name).env
       
    else:
        env_name = 'FrozenLake8x8-v1'
        env = gym.make(env_name).env
       
    nS = env.observation_space.n         # Find number of states and actions
    nA = env.action_space.n
    P = env.P                            # Get the dictionary with all the states and actions
    val = value_iteration(P, nS, nA)[0]
    vi_policy = extract_policy(P, nS, nA, val)
    pi_value_func, pi_policy, n = policy_iteration(P, nS, nA)
    vi_total_rewards = 0
    pi_total_rewards = 0
    for m in range(M):
        vi_total_rewards += run_simulation(env, vi_policy, render)
        pi_total_rewards += run_simulation(env, pi_policy, render)
        
    return vi_policy, vi_total_rewards/M, pi_value_func, pi_policy, pi_total_rewards/M
    
    
# Problem 6
def run_simulation(env, policy, render=False, beta = 1.0):
    """ Evaluates policy by using it to run a simulation and calculate the reward.

    Parameters:
    env (gym environment): The gym environment.
    policy (ndarray): The policy used to simulate.
    beta float: The discount factor.
    render (boolean): Whether to draw the environment.

    Returns:
    total reward (float): Value of the total reward received under policy.
    """
    obs = env.reset()
    done = False
    total_reward = 0
    k = 0
    if render == True:
        env.render(mode = 'human')
    while done == False:
        obs, reward, done, _ = env.step(int(policy[obs]))
        total_reward += reward*(beta**k)
        k += 1
        
    return total_reward

if __name__ == '__main__':
    v = value_iteration(P, 4 ,4, beta = 1, tol=1e-8, maxiter=5000)
    # policy = extract_policy(P, 4, 4, v, beta = 1.0)
    # print(compute_policy_v(P, 4, 4, policy, beta=1.0, tol=1e-8))
    # print(frozen_lake())
    # env_name = 'FrozenLake-v1'
    # env = gym.make(env_name).env
    # print(run_simulation(env, policy))
    pass