# nearest_neighbor.py
"""Volume 2: Nearest Neighbor Search.
<Name>
<Class>
<Date>
"""

import numpy as np
from scipy import linalg as lin
from scipy.spatial import KDTree
from scipy import stats
from matplotlib import pyplot as plt


# Problem 1
def exhaustive_search(X, z):
    """Solve the nearest neighbor search problem with an exhaustive search.

    Parameters:
        X ((m,k) ndarray): a training set of m k-dimensional points.
        z ((k, ) ndarray): a k-dimensional target point.

    Returns:
        ((k,) ndarray) the element (row) of X that is nearest to z.
        (float) The Euclidean distance from the nearest neighbor to z.
    """
    d = np.linalg.norm(X-z,axis=1)
    xstar = np.argmin(d)
    dstar = min(d)
    return X[xstar], dstar

# Problem 2: Write a KDTNode class.
class KDTNode:
    """Node class for K-D Trees.

    Attributes:
        left (KDTNode): a reference to this node's left child.
        right (KDTNode): a reference to this node's right child.
        value ((k,) ndarray): a coordinate in k-dimensional space.
        pivot (int): the dimension of the value to make comparisons on.
    """
    def __init__(self,x):
        if type(x)!=np.ndarray:
            raise TypeError("input is not a numpy array")
        self.left=None
        self.right=None
        self.value = x
        self.pivot = None


# Problems 3 and 4
class KDT:
    """A k-dimensional binary tree for solving the nearest neighbor problem.

    Attributes:
        root (KDTNode): the root node of the tree. Like all other nodes in
            the tree, the root has a NumPy array of shape (k,) as its value.
        k (int): the dimension of the data in the tree.
    """
    def __init__(self):
        """Initialize the root and k attributes."""
        self.root = None
        self.k = None

    def find(self, data):
        """Return the node containing the data. If there is no such node in
        the tree, or if the tree is empty, raise a ValueError.
        """
        def _step(current):
            """Recursively step through the tree until finding the node
            containing the data. If there is no such node, raise a ValueError.
            """
            if current is None:                     # Base case 1: dead end.
                raise ValueError(str(data) + " is not in the tree")
            elif np.allclose(data, current.value):
                return current                      # Base case 2: data found!
            elif data[current.pivot] < current.value[current.pivot]:
                return _step(current.left)          # Recursively search left.
            else:
                return _step(current.right)         # Recursively search right.

        # Start the recursive search at the root of the tree.
        return _step(self.root)

    # Problem 3
    def insert(self, data):
        """Insert a new node containing the specified data.

        Parameters:
            data ((k,) ndarray): a k-dimensional point to insert into the tree.

        Raises:
            ValueError: if data does not have the same dimensions as other
                values in the tree.
            ValueError: if data is already in the tree
        """
        
        if self.root == None:
            self.root = KDTNode(data)
            self.root.pivot = 0
            self.k = len(data)
            return self
        
        if self.root != None:  #Check if the 
            if len(data) != self.k:
                raise ValueError("Data to be inserted is not in R^"+str(self.k))        
        
        new_node = KDTNode(data)
        def _step(parent,current): 
            if current is None:   #If we are in a leaf
                if data[parent.pivot]<parent.value[parent.pivot]:  #Go left
                    parent.left = new_node
                    new_node.pivot = (parent.pivot+1) % self.k
                else:
                    parent.right = new_node                        #Go right
                    new_node.pivot = (parent.pivot+1) % self.k
                return self
            if np.allclose(current.value,data):
                raise ValueError("data is already in the tree")
            if current.value[current.pivot]>data[current.pivot]: #pick your nose
                return _step(current, current.left)
            if current.value[current.pivot]<data[current.pivot]: #lick your eye
                return _step(current, current.right)
            if current.value[current.pivot]==data[current.pivot]:#kiss a frog
                return _step(current, current.right)

        return _step(self.root,self.root)


    # Problem 4
    def query(self, z):
        """Find the value in the tree that is nearest to z.

        Parameters:
            z ((k,) ndarray): a k-dimensional target point.

        Returns:
            ((k,) ndarray) the value in the tree that is nearest to z.
            (float) The Euclidean distance from the nearest neighbor to z.
        """
        xstar = self.root.value
        dstar = lin.norm(xstar-z)
        def KDSearch(current, nearest, dstar):
            
            if current is None:
                return (nearest, dstar)
            x = current.value
            i = current.pivot
           
            if lin.norm(x-z) < dstar:
                nearest = current
                dstar = lin.norm(x-z)
            
            if z[i]<x[i]:
                nearest, dstar = KDSearch(current.left, nearest, dstar)
                if z[i] + dstar > x[i] or np.allclose(z[i] + dstar, x[i]) == True:
                    nearest, dstar = KDSearch(current.right,nearest,dstar)
          
            else:
                nearest, dstar = KDSearch(current.right,nearest, dstar)
              
                if z[i]-dstar < x[i] or np.allclose(z[i]-dstar, x[i])==True:
                    nearest, dstar = KDSearch(current.left,nearest,dstar)
           
            return (nearest, dstar)
        
        node, dstar = KDSearch(self.root,self.root,lin.norm(z-self.root.value))
        
        return node.value, dstar



    def __str__(self):
        """String representation: a hierarchical list of nodes and their axes.

        Example:                           'KDT(k=2)
                    [5,5]                   [5 5]   pivot = 0
                    /   \                   [3 2]   pivot = 1
                [3,2]   [8,4]               [8 4]   pivot = 1
                    \       \               [2 6]   pivot = 0
                    [2,6]   [7,5]           [7 5]   pivot = 0'
        """
        if self.root is None:
            return "Empty KDT"
        nodes, strs = [self.root], []
        while nodes:
            current = nodes.pop(0)
            strs.append("{}\tpivot = {}".format(current.value, current.pivot))
            for child in [current.left, current.right]:
                if child:
                    nodes.append(child)
        return "KDT(k={})\n".format(self.k) + "\n".join(strs)


# Problem 5: Write a KNeighborsClassifier class.
class KNeighborsClassifier:
    """A k-nearest neighbors classifier that uses SciPy's KDTree to solve
    the nearest neighbor problem efficiently.
    """
    def __init__(self, n_neighbors):
        self.n_neighbors=n_neighbors
    
    def fit(self, X, y):
        self.KDTree=KDTree(X)
        self.label=y
    
    def predict(self, z):
        distances, indices = self.KDTree.query(z, k=self.n_neighbors) #distanes and indices
        indices = np.array(indices)
        indices = indices.astype(int)
        return stats.mode(self.label[indices])[0][0]
    
# Problem 6
def prob6(n_neighbors, filename="mnist_subset.npz"):
    """Extract the data from the given file. Load a KNeighborsClassifier with
    the training data and the corresponding labels. Use the classifier to
    predict labels for the test data. Return the classification accuracy, the
    percentage of predictions that match the test labels.

    Parameters:
        n_neighbors (int): the number of neighbors to use for classification.
        filename (str): the name of the data file. Should be an npz file with
            keys 'X_train', 'y_train', 'X_test', and 'y_test'.

    Returns:
        (float): the classification accuracy.
    """
    data = np.load("mnist_subset.npz")
    X_train = data["X_train"].astype(np.float) # Training data
    y_train = data["y_train"] # Training labels
    X_test = data["X_test"].astype(np.float) # Test data
    y_test = data["y_test"]    
    
    knc = KNeighborsClassifier(n_neighbors)
    knc.fit(X_train, y_train)
    labels = [knc.predict(k) for k in X_test]
    
    correct = 0
    for i in range(len(labels)):
        if y_test[i] == labels[i]:
            correct += 1 
    accuracy = correct / len(labels)
    
    return accuracy
    
    plt.imshow(X_test[0].reshape((28,28)), cmap="gray")
    plt.show()


def testkdt():
    kdt=KDT()
    kdt.insert(np.array([3,5,7]))
    kdt.insert(np.array([1,4,6]))
    kdt.insert(np.array([4,4,3]))
    kdt.insert(np.array([1,5,7]))
    kdt.insert(np.array([4,3,1]))
    kdt.insert(np.array([2,7,8]))
    kdt.insert(np.array([5,5,5]))
    return kdt

def randkdt(n):
    kdt=KDT()
    for i in range(n):
        r=np.random.randint(0,10,(4,1))
        kdt.insert()
    return kdt