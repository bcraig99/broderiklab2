# object_oriented.py
"""Python Essentials: Object Oriented Programming.
Broderik S> Craig
Math 321
"""

import math

class Backpack:
    """A Backpack object class. Has a name and a list of contents.

    Attributes:
        name (str): the name of the backpack's owner.
        contents (list): the contents of the backpack.
        color (str): the color of the backpack.
        max_size (int): the max number of objects the backpack can hold.
    """

    # Problem 1: Modify __init__() and put(), and write dump().
    def __init__(self, name, color, max_size=5):
        """Set the name and initialize an empty list of contents.

        Parameters:
            name (str): the name of the backpack's owner.
            color (str): the color of the backpack.
            max_size (int): the max number of objects the backpack can hold.
        """
        self.name = name
        self.contents = []
        self.color = color
        self.max_size = max_size

    def put(self, item):
        """Add an item to the backpack's list of contents."""
        if len(self.contents)<self.max_size:
            self.contents.append(item)
        else:
            print('No Room!')
            
    def take(self, item):
        """Remove an item from the backpack's list of contents."""
        self.contents.remove(item)
        
    def dump(self):
        """Remove all items from the backpack"""
        self.contents=[]

    # Magic Methods -----------------------------------------------------------

    # Problem 3: Write __eq__() and __str__().
    def __add__(self, other):
        """Add the number of contents of each Backpack."""
        return len(self.contents) + len(other.contents)

    def __lt__(self, other):
        """Compare two backpacks. If 'self' has fewer contents
        than 'other', return True. Otherwise, return False.
        """
        return len(self.contents) < len(other.contents)

    def __eq__(self, other):
        """Compare two backpacks. if 'self' has the same number of contents,
        name, and color as 'other', return True. Otherwise, return False.
        """
        return len(self.contents)==len(other.contents) and self.name==other.name and self.color==other.color
            
    
    def __str__(self):
        """Outputs the owner, color, size, max size, and contents of 'self"""
        return str('Owner:', '\t\t', self.name, '\n', 'Color:', '\t\t', self.color, '\n', 'Size:', '\t\t', len(self.contents), '\n', 'Max Size:', '\t', self.max_size, '\n', 'Contents:', '\t', self.contents)

    
def test_backpack():
    testpack = Backpack("Barry", "black") # Instantiate the object.
    if testpack.name != "Barry": # Test an attribute.
        print("Backpack.name assigned incorrectly")
    for item in ["pencil", "pen", "paper", "computer"]:
        testpack.put(item) # Test a method.
    print("Contents:", testpack.contents)








# An example of inheritance. You are not required to modify this class.
class Knapsack(Backpack):
    """A Knapsack object class. Inherits from the Backpack class.
    A knapsack is smaller than a backpack and can be tied closed.

    Attributes:
        name (str): the name of the knapsack's owner.
        color (str): the color of the knapsack.
        max_size (int): the maximum number of items that can fit inside.
        contents (list): the contents of the backpack.
        closed (bool): whether or not the knapsack is tied shut.
    """
    def __init__(self, name, color):
        """Use the Backpack constructor to initialize the name, color,
        and max_size attributes. A knapsack only holds 3 item by default.

        Parameters:
            name (str): the name of the knapsack's owner.
            color (str): the color of the knapsack.
            max_size (int): the maximum number of items that can fit inside.
        """
        Backpack.__init__(self, name, color, max_size=3)
        self.closed = True

    def put(self, item):
        """If the knapsack is untied, use the Backpack.put() method."""
        if self.closed:
            print("I'm closed!")
        else:
            Backpack.put(self, item)

    def take(self, item):
        """If the knapsack is untied, use the Backpack.take() method."""
        if self.closed:
            print("I'm closed!")
        else:
            Backpack.take(self, item)

    def weight(self):
        """Calculate the weight of the knapsack by counting the length of the
        string representations of each item in the contents list.
        """
        return sum(len(str(item)) for item in self.contents)


# Problem 2: Write a 'Jetpack' class that inherits from the 'Backpack' class.

class Jetpack(Backpack):
    """A Jetpack object class. Inherits from the Backpack class. Holds items 
        and fuel. Uses fuel to fly

    Attributes:
        name (str): the name of the backpack's owner.
        contents (list): the contents of the backpack.
        color (str): the color of the backpack.
        max_size (int): the max number of objects the backpack can hold.
        fuel (int): the amount of fuel currently in the jetpack
    """

    # Problem 1: Modify __init__() and put(), and write dump().
    def __init__(self, name, color, max_size=2, fuel=10):
        """Set the name and initialize an empty list of contents.

        Parameters:
            name (str): the name of the backpack's owner.
            color (str): the color of the backpack.
            max_size (int): the max number of objects the backpack can hold.
            fuel (int): the amount of fuel currently in the jetpack.
        """
        Backpack.__init__(self, name, color, max_size)
        self.fuel = fuel

    def fly(self, fuel):
        """Accepts a value of fuel to be used and throws an error if not enough fuel"""
        if self.fuel>fuel:
            self.fuel-=fuel
        else:
            print('Not enough fuel!')
        
    def dump(self):
        "dumps everything in the backpack portion and the fuel"
        Backpack.dump(self)
        self.fuel = 0
            
            
# Problem 4: Write a 'ComplexNumber' class.
class ComplexNumber:
    """A Complex number object class. Has a real and imaginary component and a list of functions
        Attributes:
            real component: For a+bi, a is the real component
            imaginary component: For a+bi, bi is the imaginary component
            
            
    """
    
    def __init__(self, real, imag):
        """Set the real and imaginary components of the complex number.
        
        Parameters:
            real (int): the real component of the complex number
            imag (int): the imaginary component of the complex number
        
        """
        self.real = real
        self.imag = imag
        
    def conjugate(self):
        return ComplexNumber(self.real , -self.imag) #return the new ComplexNumber object
    
    # Magic Methods ----------------------------------------------------------
    
    def __str__(self):
        """Returns a string showing the complex number in its full representation"""
        # a=self.real    
        # b=self.imag
        # print(a,b)
        if int(self.imag)>=0:
            x = '('+str(self.real)+'+'+str(self.imag)+'j'+')'
            # print('x=',x)
            return x
        else:
            y = '('+str(self.real)+'-'+str((-1)*self.imag)+'j'+')'
            # print('y=',y)
            return y
        
    def __abs__(self):
        """Returns the magnitude of the ComplexNumber"""
        a=self.real
        b=self.imag
        return (a**(2)+b**2)**1/2
    
    def __eq__(self,other):
        """Returns True if self and other are equal, False otherwise"""
        if self.real==other.real and self.imag == other.imag:
            return True
        else:
            return False
        
    def __add__(self,other):
         """Adds real components and imaginary components and returns a new ComplexNumber"""
         # print(self.real+other.real,self.imag+other.imag)
         return ComplexNumber(self.real+other.real,self.imag+other.imag)
    
    def __sub__(self,other):
        """Adds real components and imaginary components and returns a new ComplexNumber"""
        return ComplexNumber(self.real-other.real,self.imag-other.imag)
    
    def __mul__(self,other):
        """Returns the product of two complex numbers"""
        a=self.real
        b=self.imag
        c=other.real
        d=other.imag
        return ComplexNumber(a*c-b*d, a*d+b*c)
 
    def __truediv__(self,other):
        """Returns the quotient of two comlex numbers"""
        c=other.real
        d=other.imag
        numerator = self.__mul__(other.conjugate())
        denominator = c**2+d**2
        return ComplexNumber(numerator.real/denominator, numerator.imag/denominator)
    
def test_ComplexNumber(a, b):
    """Performs esoteric functions until it ultimately decides that my perfectly valid code isn't working"""
    py_cnum, my_cnum = complex(a, b), ComplexNumber(a, b)
    # Validate the constructor.
    if my_cnum.real != a or my_cnum.imag != b:
        print("__init__() set self.real and self.imag incorrectly")
    # Validate conjugate() by checking the new number's imag attribute.
    if py_cnum.conjugate().imag != my_cnum.conjugate().imag:
        print("conjugate() failed for", py_cnum)
    # Validate __str__().
    if str(py_cnum) != str(my_cnum):
        print("__str__() failed for", py_cnum)
    # ...