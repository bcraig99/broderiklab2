09/13/21 22:07

Problem 1 (10 points):
Score += 10

Problem 2 (10 points):
The Jetpack class must inherit from the Backpack class!
Score += 0

Problem 3 (10 points):
TypeError: __str__() missing 1 required positional argument: 'other'

Problem 4 (15 points):
AttributeError: module 'BroderikCraig.ObjectOriented.object_oriented' has no attribute 'ComplexNumber'

Code Quality (5 points):
Score += 5

Total score: 15/50 = 30.0%

-------------------------------------------------------------------------------

09/16/21 12:33

Problem 1 (10 points):
Score += 10

Problem 2 (10 points):
Score += 10

Problem 3 (10 points):
TypeError: __str__ returned non-string (type NoneType)

Problem 4 (15 points):
ComplexNumber.__abs__() failed
	Correct response: 54.20332093147061
	Student response: 1469.0
Score += 13

Code Quality (5 points):
Score += 5

Total score: 38/50 = 76.0%

-------------------------------------------------------------------------------

10/02/21 11:57

Problem 1 (10 points):
Score += 10

Problem 2 (10 points):
Score += 10

Problem 3 (10 points):
TypeError: str() takes at most 3 arguments (19 given)

Problem 4 (15 points):
ComplexNumber.__abs__() failed
	Correct response: 49.57822102496216
	Student response: 1229.0
Score += 13

Code Quality (5 points):
Score += 5

Total score: 38/50 = 76.0%

-------------------------------------------------------------------------------

