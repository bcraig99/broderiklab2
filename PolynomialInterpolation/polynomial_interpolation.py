# polynomial_interpolation.py
"""Volume 2: Polynomial Interpolation.
<Name>
<Class>
<Date>
"""
import numpy as np
import sympy as sy
from matplotlib import pyplot as plt
from scipy.interpolate import BarycentricInterpolator as BI
import numpy.linalg as la
from numpy.fft import fft

# Problems 1 and 2
def lagrange(xint, yint, points):
    """Find an interpolating polynomial of lowest degree through the points
    (xint, yint) using the Lagrange method and evaluate that polynomial at
    the specified points.

    Parameters:
        xint ((n,) ndarray): x values to be interpolated.
        yint ((n,) ndarray): y values to be interpolated.
        points((m,) ndarray): x values at which to evaluate the polynomial.

    Returns:
        ((m,) ndarray): The value of the polynomial at the specified points.
    """
    x = sy.symbols('x')
    n = len(xint)
    m = len(points)
    #create denominator
    denom = []
    for j in range(n):
        denom.append(np.product([xint[j]-xint[k] for k in range(n) if k != j]))
    #create numerator
    L = []
    for j in range(n):
        L.append(np.product([x-xint[k] for k in range(n) if k != j])/denom[j])
    #lambdify L
    for i, func in enumerate(L):
        L[i] = sy.lambdify(x, func)
    p = np.sum([yint[j]*L[j](x) for j in range(n)])    
    p = sy.lambdify(x,p)
    result = [p(point) for point in points]
    return result

# Problems 3 and 4
class Barycentric:
    """Class for performing Barycentric Lagrange interpolation.

    Attributes:
        w ((n,) ndarray): Array of Barycentric weights.
        n (int): Number of interpolation points.
        x ((n,) ndarray): x values of interpolating points.
        y ((n,) ndarray): y values of interpolating points.
    """

    def __init__(self, xint, yint):
        """Calculate the Barycentric weights using initial interpolating points.

        Parameters:
            xint ((n,) ndarray): x values of interpolating points.
            yint ((n,) ndarray): y values of interpolating points.
        """
        self.xint = xint
        self.yint = yint
        self.n = len(xint)
        x, j = sy.symbols('x j')
        n = len(xint)
        C = (np.max(xint) - np.min(xint)) / 4
        w = []
        for j in range(len(xint)):
            w.append(1/np.product([xint[j] - xint[k] for k in range(n) if k != j]/C))
        self.w = w  
        
    def __call__(self, points):
        """Using the calcuated Barycentric weights, evaluate the interpolating polynomial
        at points.

        Parameters:
            points ((m,) ndarray): Array of points at which to evaluate the polynomial.

        Returns:
            ((m,) ndarray): Array of values where the polynomial has been computed.
        """
        
        x = sy.symbols('x')
        n = len(self.xint)
        
        num = np.sum([self.w[j]*self.yint[j]/(x-self.xint[j]) for j in range(n)])
        denom = np.sum([self.w[j]/(x-self.xint[j]) for j in range(n)])
        p = num/denom
        p = sy.simplify(p)
        p = sy.lambdify(x, p)
        
        solution = []
        for point in points:
            solution.append(p(point))
            
        return solution

    # Problem 4
    def add_weights(self, xint, yint):
        """Update the existing Barycentric weights using newly given interpolating points
        and create new weights equal to the number of new points.

        Parameters:
            xint ((m,) ndarray): x values of new interpolating points.
            yint ((m,) ndarray): y values of new interpolating points.
        """
        newxint = np.concatenate((xint, self.xint))
        index = np.argsort(newxint)
        newxint = newxint[index]
        newyint = np.concatenate((yint, self.yint))
        newyint = newyint[index]
        self.__init__(newxint, newyint)
        

# Problem 5
def prob5():
    """For n = 2^2, 2^3, ..., 2^8, calculate the error of intepolating Runge's
    function on [-1,1] with n points using SciPy's BarycentricInterpolator
    class, once with equally spaced points and once with the Chebyshev
    extremal points. Plot the absolute error of the interpolation with each
    method on a log-log plot.
    """
    f = lambda x: 1/(1+25*x**2)
    errorseven = []
    errorscheby = []
    nrange = [2**i for i in range(2,8)]
    for n in nrange:
        pts = np.linspace(-1,1,n)
        poly = BI(pts)
        poly.set_yi(f(pts))
        errorseven.append(la.norm((f(pts),poly(pts)), ord = np.inf))
        
        pts = np.array([np.cos(j * np.pi/n) for j in range(n+1)])
        poly = BI(pts)
        poly.set_yi(f(pts))
        errorscheby.append(la.norm((f(pts),poly(pts)), ord = np.inf))
        
    plt.loglog(nrange, errorseven, color = 'deeppink', label = 'Evenly Spaced Points Error')
    plt.loglog(nrange, errorscheby, color = 'blue', label = 'Chebyshev Extremal Point')
    plt.legend()
    plt.title('Errors of Barycentric Interpolation')
    plt.show()

# Problem 6
def chebyshev_coeffs(f, n):
    """Obtain the Chebyshev coefficients of a polynomial that interpolates
    the function f at n points.

    Parameters:
        f (function): Function to be interpolated.
        n (int): Number of points at which to interpolate.

    Returns:
        coeffs ((n+1,) ndarray): Chebyshev coefficients for the interpolating polynomial.
    """
    y = np.cos((np.pi * np.arange(2*n)) / n)
    samples = f(y)
    coeffs = np.real(fft(samples))[:n+1] / n
    coeffs[0] = coeffs[0]/2
    coeffs[n] = coeffs[n]/2
    return coeffs

    
    
    
    

# Problem 7
def prob7(n):
    """Interpolate the air quality data found in airdata.npy using
    Barycentric Lagrange interpolation. Plot the original data and the
    interpolating polynomial.

    Parameters:
        n (int): Number of interpolating points to use.
    """
    data = np.load("airdata.npy")
    fx = lambda a, b, n: .5*(a+b + (b-a) * np.cos(np.arange(n+1) * np.pi / n))
    a, b = 0, 366 - 1/24
    domain = np.linspace(0, b, 8784)
    points = fx(a, b, n)
    temp = np.abs(points - domain.reshape(8784, 1))
    temp2 = np.argmin(temp, axis=0)
    poly = BI(domain[temp2], data[temp2])
    
    f = plt.figure()
    f.set_figheight(6)
    f.set_figwidth(5)
    ax1 = plt.subplot(311)
    ax1.scatter(domain, data, s = .2)
    plt.title('Original Data')
    
    to_plot = poly(domain)
    ax2 = plt.subplot(312)
    ax2.plot(domain, to_plot)
    plt.title('Polynomial Approx')
    
    ax3 = plt.subplot(313)
    ax3.scatter(domain, data, s = .2, color = 'fuchsia')
    ax3.plot(domain, to_plot)
    plt.title('Superimposed')
    
    plt.tight_layout()
    plt.show()
if __name__ == '__main__':
    
    pass
    
    
    # domain = np.linspace(-1, 1, 1000)
    # f = lambda x: 1 / (1+ 25*(x**2))
    # xs = np.linspace(-1, 1, 5)
    # ys = f(xs)
    # b = Barycentric(xs, ys)
    # plt.plot(domain, f(domain), color = "darkslategray")
    # plt.plot(domain, b(domain), color = "Fuchsia")
    # plt.show()

    # n = 11
    # runge = lambda x: 1 / (1 + 25 * x**2)
    # xvals_original = np.linspace(-1, 1, n)
    # xvals_1 = xvals_original[1::2]
    # xvals_2 = xvals_original[::2]
    # domain = np.linspace(-1, 1, 1000)
    # bary = Barycentric(xvals_1, runge(xvals_1))
    # bary_2 = Barycentric(xvals_original, runge(xvals_original))
    # plt.plot(domain, bary_2(domain),linewidth=6, label='Not added')
    # plt.plot(domain, runge(domain), label='Original')
    # plt.plot(domain, bary(domain), label='Odd Points, n = ' + str(n))
    # bary.add_weights(xvals_2, runge(xvals_2))
    # plt.plot(domain, bary(domain),'k', label='All points, n = ' + str(n))
    # plt.legend(loc='best')
    # plt.show()






