"""Volume 2: Simplex

<Name>
<Date>
<Class>
"""

import numpy as np


# Problems 1-6
class SimplexSolver(object):
    """Class for solving the standard linear optimization problem

                        minimize        c^Tx
                        subject to      Ax <= b
                                         x >= 0
    via the Simplex algorithm.
    """
    # Problem 1
    def __init__(self, c, A, b):
        """Check for feasibility and initialize the dictionary.

        Parameters:
            c ((n,) ndarray): The coefficients of the objective function.
            A ((m,n) ndarray): The constraint coefficients matrix.
            b ((m,) ndarray): The constraint vector.

        Raises:
            ValueError: if the given system is infeasible at the origin.
        """
        if np.any(b<0):
            raise ValueError('The given system is infeasible at the origin')
        self._generatedictionary(c, A, b)

    # Problem 2
    def _generatedictionary(self, c, A, b):
        """Generate the initial dictionary.

        Parameters:
            c ((n,) ndarray): The coefficients of the objective function.
            A ((m,n) ndarray): The constraint coefficients matrix.
            b ((m,) ndarray): The constraint vector.
        """
        m,n = A.shape
        Abar = np.hstack([A, np.eye(m)])  #Stack A with I
        cbar = np.append(c,[0]*m)         #fill up c with zeros
        bbar = np.append(0,b)             #add zero to beginning of b
        right = np.vstack([cbar,-Abar])   #stack cbar
        self.D = np.insert(right,0,bbar,axis = 1) #stick bbar onto right


    # Problem 3a
    def _pivot_col(self):
        """Return the column index of the next pivot column.
        """
        # print(self.D)
        for i, item in enumerate(self.D[0]): #iterate through first row
            # print(i,item)
            if i == 0:                       #skip first index
                continue
            if item < 0:                     #stop at first negative entry
                self.pivot_col = i
                return i
        # self.pivot_col = len(self.D[0])-1
        # return len(self.D[0]-1)
        
    # Problem 3b
    def _pivot_row(self, index):
        """Determine the row index of the next pivot row using the ratio test
        (Bland's Rule).
        """
        
        if np.all(self.D[:,index] >= 0):
            raise ValueError('The problem is unbounded and has no solution')
            
        top = -self.D[1:,0].copy()

        
        bottom = self.D[1:,index].copy()
        
        mask = bottom >= 0
        
        for i, element in enumerate(bottom):
            if element >= 0:
                bottom[i]=np.min(top)
 
        ratio = top/bottom
        ratio[mask] = np.max(ratio)+1
        
        # for i, element in enumerate(ratio):
        #     if element <= 0:
        #         ratio[i]=99999
        self.pivot_row = np.argmin(ratio)+1
        return np.argmin(ratio)

    # Problem 4
    def pivot(self):
        """Select the column and row to pivot on. Reduce the column to a
        negative elementary vector.
        """
        
        m, n = self.pivot_row, self.pivot_col
        self.D[m] = self.D[m]/(-self.D[m, n])
        for i in range(self.D.shape[0]):
            if i == m:
                continue
            else:
                self.D[i] += self.D[i,n]*self.D[m]
            
        
    # Problem 5
    def solve(self):
        """Solve the linear optimization problem.

        Returns:
            (float) The minimum value of the objective function.
            (dict): The basic variables and their values.
            (dict): The nonbasic variables and their values.
        """
        
        m,n = np.shape(self.D)
        boo = False
        # print(self.D)
        boo = np.all(self.D[0,1:]>=0)
        while boo == False:
            self._pivot_col()
            self._pivot_row(self.pivot_col)
            self.pivot()
            # print(self.pivot_row)
            # print(self.D)
            if np.all(self.D[0,1:]>=0):
                boo = True
            
                
        solution = {}
        for i in range(1,n):
            if self.D[0,i]==0:
                index = np.argmin(self.D[:,i])
                solution[i-1]=np.round(self.D[index,0],10)
                
        slack = {}
        for i in range(n-1):
            if i not in solution.keys():
                slack[i]=0
        return self.D[0,0], solution, slack
        
# Problem 6
def prob6(filename='productMix.npz'):
    """Solve the product mix problem for the data in 'productMix.npz'.

    Parameters:
        filename (str): the path to the data file.

    Returns:
        ((n,) ndarray): the number of units that should be produced for each product.
    """
    data = np.load(filename)
    c = -data['p']
    m, n = np.shape(data['A'])
    d = data['d']
    M = data['m']
    A = np.concatenate((data['A'], np.eye(n)), axis=0)
    b = np.concatenate((M,d))
    solver = SimplexSolver(c, A, b)
    sol = solver.solve()
    solution = []
    for i in range(4):
        solution.append(sol[1][i])
    return solution
    
if __name__ == '__main__':
    # c = np.array([-3,-2])
    # A = np.array([1,-1,3,1,4,3]).reshape((3,2))
    # b = np.array([2,5,7])
    # S = SimplexSolver(c,A,b)
    # S._generatedictionary(c, A, b)
    # S._pivot_col()
    # S._pivot_row(S._pivot_col())
    # S.pivot()
    # k = prob6()
    # print(k)
    c = np.array([-10, 57, 9, 24])
    A = np.array([[0.5,-1.5, -0.5, 1], [0.5, -5.5, -2.5, 9], [1, 0, 0, 0]])
    b = np.array([0, 0, 1])
    S = SimplexSolver(c, A, b)
    sol = S.solve()
    # print(sol)
    
    
    c = np.array([3,-1])
    A = np.array([[0,1], [-2, 3]])
    b = np.array([4, 6])
    S = SimplexSolver(c, A, b)
    sol = S.solve()
    # print(sol)
    
    # c = np.array([-5,-2])
    # A = np.array([[5,3], [3, 5], [4, -3]])
    # b = np.array([15, 15, -12])
    # S = SimplexSolver(c, A, b)
    # sol = S.solve()
    # print(sol)
    
    c = np.array([-5,4])
    A = np.array([[2,-3], [1, -6], [1, 1]])
    b = np.array([4, 1, 6])
    S = SimplexSolver(c, A, b)
    sol = S.solve()
    # print(sol)
    
    c = np.array([3., 2.])
    b = np.array([2., 5., 7.])
    A = np.array([[1., -1.],
                  [3.,  1.],
                  [4.,  3.]])
    S = SimplexSolver(c, A, b)
    sol = S.solve()
    print(sol)


    pass




