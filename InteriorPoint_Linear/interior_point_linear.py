# interior_point_linear.py
"""Volume 2: Interior Point for Linear Programs.
<Name>
<Class>
<Date>
"""

import numpy as np
from scipy import linalg as la
from scipy.stats import linregress
from matplotlib import pyplot as plt
import sympy as sy


# Auxiliary Functions ---------------------------------------------------------
def starting_point(A, b, c):
    """Calculate an initial guess to the solution of the linear program
    min c^T x, Ax = b, x>=0.
    Reference: Nocedal and Wright, p. 410.
    """
    # Calculate x, lam, mu of minimal norm satisfying both
    # the primal and dual constraints.
    B = la.inv(A @ A.T)
    x = A.T @ B @ b
    lam = B @ A @ c
    mu = c - (A.T @ lam)

    # Perturb x and s so they are nonnegative.
    dx = max((-3./2)*x.min(), 0)
    dmu = max((-3./2)*mu.min(), 0)
    x += dx*np.ones_like(x)
    mu += dmu*np.ones_like(mu)

    # Perturb x and mu so they are not too small and not too dissimilar.
    dx = .5*(x*mu).sum()/mu.sum()
    dmu = .5*(x*mu).sum()/x.sum()
    x += dx*np.ones_like(x)
    mu += dmu*np.ones_like(mu)

    return x, lam, mu

# Use this linear program generator to test your interior point method.
def randomLP(j,k):
    """Generate a linear program min c^T x s.t. Ax = b, x>=0.
    First generate m feasible constraints, then add
    slack variables to convert it into the above form.
    Parameters:
        j (int >= k): number of desired constraints.
        k (int): dimension of space in which to optimize.
    Returns:
        A ((j, j+k) ndarray): Constraint matrix.
        b ((j,) ndarray): Constraint vector.
        c ((j+k,), ndarray): Objective function with j trailing 0s.
        x ((k,) ndarray): The first 'k' terms of the solution to the LP.
    """
    A = np.random.random((j,k))*20 - 10
    A[A[:,-1]<0] *= -1
    x = np.random.random(k)*10
    b = np.zeros(j)
    b[:k] = A[:k,:] @ x
    b[k:] = A[k:,:] @ x + np.random.random(j-k)*10
    c = np.zeros(j+k)
    c[:k] = A[:k,:].sum(axis=0)/k
    A = np.hstack((A, np.eye(j)))
    return A, b, -c, x


# Problems --------------------------------------------------------------------
def interiorPoint(A, b, c, niter=20, tol=1e-16, verbose=False):
    """Solve the linear program min c^T x, Ax = b, x>=0
    using an Interior Point method.

    Parameters:
        A ((m,n) ndarray): Equality constraint matrix with full row rank.
        b ((m, ) ndarray): Equality constraint vector.
        c ((n, ) ndarray): Linear objective function coefficients.
        niter (int > 0): The maximum number of iterations to execute.
        tol (float > 0): The convergence tolerance.

    Returns:
        x ((n, ) ndarray): The optimal point.
        val (float): The minimum value of the objective function.
    """
    m,n = A.shape
    def setup(x,lam,mu):
        
        def construct_F(): #This will return the F vector used later
            F = np.concatenate((A.T@lam + mu -c, A@x - b)) #first 2 elements
            F = np.concatenate((F, np.diag(mu)@x)) #last element
            return F
        
        lenx,lenlam,lenmu = len(x), len(lam), len(mu)  #This will be important for Deltax,lam,mu
        F = construct_F()
        
        def search_direction():
            X = np.diag(x)
            DF1 = np.hstack((np.zeros((n,n)),A.T, np.eye(n))) #First row of DF
            DF2 = np.hstack((A,np.zeros((m,m)),np.zeros((m,n)))) #Second row of DF
            DF3 = np.hstack((np.diag(mu),np.zeros((n,m)),X)) #Third row of DF
            DF = np.vstack((DF1,DF2,DF3)) #DF!
            bottom = ((x.T@mu)/n)*np.ones(len(np.diag(mu)@x))/10 #bottom of the b vector
            right = np.append(np.zeros(len(F)-len(bottom)),bottom) #the whole b vector
            sol = la.lu_solve(la.lu_factor(DF),-F+right) #Dude, np.linalg does it for me
            # return sol
            return sol[:lenx], sol[lenx:lenx+lenlam], sol[lenx+lenlam:] #Dx, Dlam, Dmu
            
        # direction = search_direction(F, a, b, c)
        Dx, Dlam, Dmu = search_direction()
        
        def search_len():
            if Dmu.all() >= 0 == True: #if Dmu is nonnegative
                alphamax = 1
            else:
                alphamax = min(-mu[Dmu<0]/Dmu[Dmu<0]) #masking to extract the elements that are negative
            
            if Dx.all() >= 0 == True:
                deltamax = 1
            else:
                deltamax = min(-x[Dx<0]/Dx[Dx<0]) #same as above
            
            alpha = min(1, .95*alphamax)
            delta = min(1, .95*deltamax)
            return alpha, delta
        alpha, delta = search_len()
        
        return delta*Dx, alpha*Dlam, alpha*Dmu #These will be important in the loop
    
    x, lam, mu = starting_point(A,b,c)
    for _ in range(niter):
        deltaDx, alphaDlam, alphaDmu = setup(x,lam,mu) #told you they were important
        xnext = x + deltaDx #iterate to next x, lam, mu
        lamnext = lam + alphaDlam
        munext =  mu + alphaDmu
        nu = ((xnext.T)@munext)/n
        if nu <= tol:
            break
        x = xnext
        lam = lamnext
        mu = munext
        
    return x, c.T@x #optimization point and optimal value
    
    # return search_direction(construct_F(x,lam,mu),a,b,c)
    
def leastAbsoluteDeviations(filename='simdata.txt'):
    """Generate and show the plot requested in the lab."""
    data = np.loadtxt(filename)
    m = data.shape[0]
    n = data.shape[1] - 1
    c = np.zeros(3*m + 2*(n + 1))
    c[:m] = 1
    y = np.empty(2*m)
    y[::2] = -data[:, 0]
    y[1::2] = data[:, 0]
    x = data[:, 1:]
        
    A = np.ones((2*m, 3*m + 2*(n + 1)))
    A[::2, :m] = np.eye(m)
    A[1::2, :m] = np.eye(m)
    A[::2, m:m+n] = -x
    A[1::2, m:m+n] = x
    A[::2, m+n:m+2*n] = x
    A[1::2, m+n:m+2*n] = -x
    A[::2, m+2*n] = -1
    A[1::2, m+2*n+1] = -1
    A[:, m+2*n+2:] = -np.eye(2*m, 2*m)
    
    sol = interiorPoint(A, y, c, niter=10)[0]
        
    beta = sol[m:m+n] - sol[m+n:m+2*n]
    b = sol[m+2*n] - sol[m+2*n+1]
    
    slope, intercept = linregress(data[:,1], data[:,0])[:2]
    domain = np.linspace(0,10,200)
    
    plt.plot(domain,domain*beta+b,color = 'k', label = 'LAD Method')
    plt.plot(domain, domain*slope + intercept, label = 'Least Squares')
    x0 = data[:,1]
    y0 = data[:,0]
    plt.scatter(x0,y0,color = 'deeppink', label = 'simdata.txt Points')
    plt.title('Linear Regression')
    plt.legend()
    plt.show()
    
def test():
    for _ in range(200):
        j = np.random.randint(3,10)
        A,b,c,x = randomLP(j,j)
        point, value = interiorPoint(A,b,c)
        if np.allclose(x,point[:j]) == False:
            print("oh no")
            print(j)
    for _ in range(150):
        n = np.random.randint(3,10)
        m = np.random.randint(n,20)
        A,b,c,x = randomLP(m,n)
        point, value = interiorPoint(A,b,c)
        if np.allclose(x,point[:n]) == False:
            print("oh no")
            print(m, n)
    