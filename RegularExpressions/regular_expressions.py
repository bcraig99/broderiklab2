# regular_expressions.py
"""Volume 3: Regular Expressions.
<Name>
<Class>
<Date>
"""

import re

# Problem 1
def prob1():
    """Compile and return a regular expression pattern object with the
    pattern string "python".

    Returns:
        (_sre.SRE_Pattern): a compiled regular expression pattern object.
    """
    pattern = re.compile('python')
    return pattern

# Problem 2
def prob2():
    """Compile and return a regular expression pattern object that matches
    the string "^{@}(?)[%]{.}(*)[_]{&}$".

    Returns:
        (_sre.SRE_Pattern): a compiled regular expression pattern object.
    """
    pattern = re.compile(r"\^\{@\}\(\?\)\[%\]\{\.\}\(\*\)\[_\]\{&\}\$")
    return pattern


# Problem 3
def prob3():
    """Compile and return a regular expression pattern object that matches
    the following strings (and no other strings).

        Book store          Mattress store          Grocery store
        Book supplier       Mattress supplier       Grocery supplier

    Returns:
        (_sre.SRE_Pattern): a compiled regular expression pattern object.
    """
    pattern = re.compile(r"^(Book|Mattress|Grocery) (supplier|store)$")
    return pattern

# Problem 4
def prob4():
    """Compile and return a regular expression pattern object that matches
    any valid Python identifier.

    Returns:
        (_sre.SRE_Pattern): a compiled regular expression pattern object.
    """
    pattern = re.compile(r"^[_|a-zA-Z](\w|_){0,} {0,}(|= {0,}(\d{0,}(|\.\d{0,})|'[^']{0,}'|[a-zA-z_](\w|_){0,}))$")
    return pattern

# Problem 5
def prob5(code):
    """Use regular expressions to place colons in the appropriate spots of the
    input string, representing Python code. You may assume that every possible
    colon is missing in the input string.

    Parameters:
        code (str): a string of Python code without any colons.

    Returns:
        (str): code, but with the colons inserted in the right places.
    """
    keywords = r"(^\s*(if|else|elif|for|while|try|except|finally|with|def|class)[\n]*"
    
    pattern = re.compile(keywords, re.MULTILINE)
    
    new_code = pattern.sub(r"\1:", code)
    
    return new_code
    
# Problem 6
def prob6(filename="fake_contacts.txt"):
    """Use regular expressions to parse the data in the given file and format
    it uniformly, writing birthdays as mm/dd/yyyy and phone numbers as
    (xxx)xxx-xxxx. Construct a dictionary where the key is the name of an
    individual and the value is another dictionary containing their
    information. Each of these inner dictionaries should have the keys
    "birthday", "email", and "phone". In the case of missing data, map the key
    to None.

    Returns:
        (dict): a dictionary mapping names to a dictionary of personal info.
    """

    birthday = re.compile(r"^(\d{1,2})/(\d{1,2})/(\d{2,4})")
    # print(filename.findall(birthday))
    email = re.compile(r"^\w(\w|_|.)*@")
    
    phone = re.compile(r"^\((\d)*")
    
    while True:
            try:
                with open(filename,encoding='utf-8') as infile:
                    stuff=infile.read()
                break  
            except (FileNotFoundError, FileExistsError, IOError, SyntaxError):
                infile=input("Please input a valid filename:")
                
    contacts = dict()
    contents = stuff.strip('\n').split('\n')
    print(email.findall(contents[4]))
    # for i in range(len(contents)):
    #     line = list(contents[i].split())
    #     print(line)
    #     contacts[line[0]+' '+line[1]] = {}
    #     contacts[line[0]+' '+line[1]]['birthday'] = line[2]
    #     contacts[line[0]+' '+line[1]]['email'] = line[4]
    #     contacts[line[0]+' '+line[1]]['phone'] = line[3]
    # print(contacts)
