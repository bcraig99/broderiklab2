# markov_chains.py
"""Volume 2: Markov Chains.
<Name>
<Class>
<Date>
"""

import numpy as np
from scipy import linalg as la


class MarkovChain:
    """A Markov chain with finitely many states.

    Attributes:
        A ((n,n) ndarray): the column-stochastic transition matrix for a
            Markov chain with n states.
        states (list(str)): a list of n labels corresponding to the n states.
            If not provided, the labels are the indices 0, 1, ..., n-1.\
        m: the integer dimension of A
        map: a dictionary mapping each label to its index
    """
    # Problem 1
    def __init__(self, A, states=None):
        """Check that A is column stochastic and construct a dictionary
        mapping a state's label to its index (the row / column of A that the
        state corresponds to). Save the transition matrix, the list of state
        labels, and the label-to-index dictionary as attributes.

        Parameters:
        A ((n,n) ndarray): the column-stochastic transition matrix for a
            Markov chain with n states.
        states (list(str)): a list of n labels corresponding to the n states.
            If not provided, the labels are the indices 0, 1, ..., n-1.

        Raises:
            ValueError: if A is not square or is not column stochastic.

        Example:
            >>> MarkovChain(np.array([[.5, .8], [.5, .2]], states=["A", "B"])
        corresponds to the Markov Chain with transition matrix
                                   from A  from B
                            to A [   .5      .8   ]
                            to B [   .5      .2   ]
        and the label-to-index dictionary is {"A":0, "B":1}.
        """
        self.m,self.n=np.shape(A) #size of matrix
        if self.m != self.n:      #make sure it's square
            raise ValueError("A is not square")
        for i in np.sum(A, axis=0): #make sure it's stochastic
            if round(i,2) != 1:
                raise ValueError("A is not column stochastic. Column sums are "+str(np.sum(A, axis=0)))
       
        indeces = np.arange(self.m)
        if states==None:
            self.labels = indeces
        else:
            if len(states) != self.m:
                raise ValueError("States is not the same length as the number of columns")
            self.labels = states
        
        self.map = {}
        for i in range(self.m):
            self.map[self.labels[i]]=i
            
        self.A = A
        
        

    # Problem 2
    def transition(self, state):
        """Transition to a new state by making a random draw from the outgoing
        probabilities of the state with the specified label.

        Parameters:
            state (str): the label for the current state.

        Returns:
            (str): the label of the state to transitioned to.
        """
        i = self.map[state]
        output = np.random.multinomial(1, self.A[:,i]) #Outputs a vector with just one 1 based on the probability of each entry
        k = np.argmax(output) #index of the 1
        return self.labels[k]
        

    # Problem 3
    def walk(self, start, N):
        """Starting at the specified state, use the transition() method to
        transition from state to state N-1 times, recording the state label at
        each step.

        Parameters:
            start (str): The starting state label.

        Returns:
            (list(str)): A list of N state labels, including start.
        """
        self.walk = [start]
        current = start
        for n in range(N-1): #Use transition to step through the matrix nodes
            current = self.transition(current)
            self.walk.append(current)
        
        return self.walk

    # Problem 3
    def path(self, start, stop):
        """Beginning at the start state, transition from state to state until
        arriving at the stop state, recording the state label at each step.

        Parameters:
            start (str): The starting state label.
            stop (str): The stopping state label.

        Returns:
            (list(str)): A list of state labels from start to stop.
        """
        self.path = [start]
        current = start
        while True:  #keep going till you hit the stop
             current = self.transition(current)
             self.path.append(current)
             if current == stop:
                 break
        return self.path


    # Problem 4
    def steady_state(self, tol=1e-12, maxiter=40):
        """Compute the steady state of the transition matrix A.

        Parameters:
            tol (float): The convergence tolerance.
            maxiter (int): The maximum number of iterations to compute.

        Returns:
            ((n,) ndarray): The steady state distribution vector of A.

        Raises:
            ValueError: if there is no convergence within maxiter iterations.
        """
        x = np.random.random((self.m,1))  #random vector of length n
        x = [i/sum(x) for i in x]   #make sure it sums to zero
        current = np.array(x)   #set current as the random vector
        for k in range(maxiter):
            prev = current.copy()
            current = self.A @ current
            if la.norm(prev-current,ord=1)<tol: #If the distance between the previous and the current is within tolerance
                return current
        
        raise ValueError("No convergence exists within "+str(maxiter)+" iterations.")
        

class SentenceGenerator(MarkovChain):
    """A Markov-based simulator for natural language.

    Attributes:
        (fill this out)
    """
    # Problem 5
    def __init__(self, filename):
        """Read the specified file and build a transition matrix from its
        contents. You may assume that the file has one complete sentence
        written on each line.
        """
        while True:
           try:
               with open(filename,encoding='utf-8') as infile:
                   stuff=infile.read()
               break  
           except (FileNotFoundError, FileExistsError, IOError, SyntaxError):
               infile=input("Please input a valid filename:")
        
        contents = stuff.strip('\n').split('\n')
        self.labels = list(set(stuff.strip().split())) #create list of each independent word
        self.labels.append('$top')
        self.labels.insert(0,'$tart')
        self.TransitionMatrix=np.zeros((len(self.labels),len(self.labels))) #create empty matrix
    
        self.map = {} #create a map dictionary like in MarkovChain
        for i in range(len(self.labels)):
            self.map[self.labels[i]]=i

        for line in contents: #step through each sentence
            line = line.strip().split()
            line.append('$top')
            line.insert(0,'$tart')
            for i in range(len(line)-1):
                self.TransitionMatrix[self.map[line[i+1]],self.map[line[i]]]+=1 #add 1 to the transition between word i and i+1

        self.TransitionMatrix[-1,-1]=1 #set $top-->$top as 1
        sums = np.sum(self.TransitionMatrix,axis=0)

        for i in range(len(self.labels)): #normalize
            self.TransitionMatrix[:,i] = self.TransitionMatrix[:,i]/sums[i]
        self.TransitionMatrix
        
        
    # Problem 6
    def babble(self):
        """Create a random sentence using MarkovChain.path().

        Returns:
            (str): A sentence generated with the transition matrix, not
                including the labels for the $tart and $top states.

        Example:
            >>> yoda = SentenceGenerator("yoda.txt")
            >>> print(yoda.babble())
            The dark side of loss is a path as one with you.
        """
        
        chain=MarkovChain(self.TransitionMatrix, states=self.labels) #create a markov chain with the matrix we created above
        sentence = chain.path('$tart', '$top')
        sentence.pop()
        sentence.pop(0)
        return ' '.join(sentence) #return the sentence!