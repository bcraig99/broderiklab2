# linked_lists.py
"""Volume 2: Linked Lists.
<Broderik Craig>
<Math 321 Section 3>
<9/30/21>
"""


# Problem 1
class Node:
    """A basic node class for storing data."""
    def __init__(self, data):
        """Store the data in the value attribute.
        
        Input:
            string, float, or integer
                
        Raises:
            TypeError: if data is not of type int, float, or str.
        """
        if type(data) != int and type(data) !=float and type(data) != str:
            raise TypeError("Data must be a integer, float, or string")
        self.value = data


class LinkedListNode(Node):
    """A node class for doubly linked lists. Inherits from the Node class.
    Contains references to the next and previous nodes in the linked list.
    """
    def __init__(self, data):
        """Store the data in the value attribute and initialize
        attributes for the next and previous nodes in the list.
        """
        Node.__init__(self, data)       # Use inheritance to set self.value.
        self.next = None                # Reference to the next node.
        self.prev = None                # Reference to the previous node.


# Problems 2-5
class LinkedList:
    """Doubly linked list data structure class.

    Attributes:
        head (LinkedListNode): the first node in the list.
        tail (LinkedListNode): the last node in the list.
    """
    def __init__(self):
        """Initialize the head and tail attributes by setting
        them to None, since the list is empty initially.
        """
        self.head = None
        self.tail = None
        self.len = 0

    def append(self, data):
        """Append a new node containing the data to the end of the list."""
        # Create a new node to store the input data.
        new_node = LinkedListNode(data)
        if self.head is None:
            # If the list is empty, assign the head and tail attributes to
            # new_node, since it becomes the first and last node in the list.
            self.head = new_node
            self.tail = new_node
            self.len += 1
        else:
            # If the list is not empty, place new_node after the tail.
            self.tail.next = new_node               # tail --> new_node
            new_node.prev = self.tail               # tail <-- new_node
            # Now the last node in the list is new_node, so reassign the tail.
            self.tail = new_node
            self.len += 1
            
            
    # Problem 2
    def find(self, data):
        """Return the first node in the list containing the data.

        Raises:
            ValueError: if the list does not contain the data.

        Examples:
            >>> l = LinkedList()
            >>> for x in ['a', 'b', 'c', 'd', 'e']:
            ...     l.append(x)
            ...
            >>> node = l.find('b')
            >>> node.value
            'b'
            >>> l.find('f')
            ValueError: <message>
        """
        node=self.head     #Start at the head
        while True:
            if node.value==data:  #if the node's value is the searched item, return it
                return node
               
            if node==self.tail:   #if we've reached the end of the list, stop
                break
        
            node=node.next        #iterate to the next node
        raise ValueError("data is not in the linked list")


    # Problem 2
    def get(self, i):
        """Return the i-th node in the list.

        Raises:
            IndexError: if i is negative or greater than or equal to the
                current number of nodes.

        Examples:
            >>> l = LinkedList()
            >>> for x in ['a', 'b', 'c', 'd', 'e']:
            ...     l.append(x)
            ...
            >>> node = l.get(3)
            >>> node.value
            'd'
            >>> l.get(5)
            IndexError: <message>
        """
        if i<0:
            raise ValueError("i must be a positive integer")
        if i>self.len-1:
            raise ValueError("i is outside the range of the list")
        if type(i) is not int:
            raise TypeError("i must be a positive integer")
        
        index=0               #initialize the index counter
        node=self.head        #Start at the head
        while True:
            if index==i:      #Stop if we've reached the correct node
                return node
            else:
                node=node.next #iterate to the next node
                index+=1
                
            

    # Problem 3
    def __len__(self):
        """Return the number of nodes in the list.

        Examples:
            >>> l = LinkedList()
            >>> for i in (1, 3, 5):
            ...     l.append(i)
            ...
            >>> len(l)
            3
            >>> l.append(7)
            >>> len(l)
            4
        """
        return self.len

    # Problem 3
    def __str__(self):
        """String representation: the same as a standard Python list.

        Examples:
            >>> l1 = LinkedList()       |   >>> l2 = LinkedList()
            >>> for i in [1,3,5]:       |   >>> for i in ['a','b',"c"]:
            ...     l1.append(i)        |   ...     l2.append(i)
            ...                         |   ...
            >>> print(l1)               |   >>> print(l2)
            [1, 3, 5]                   |   ['a', 'b', 'c']
        """
        # node=self.head
        stringrep=[] #Set empty list
        for i in range(len(self)): #iterate through all the elements of the list 
           stringrep.append(self.get(i).value) #use get function to call each node and its value
        return str(stringrep)

    # Problem 4
    def remove(self, data):
        """Remove the first node in the list containing the data.

        Raises:
            ValueError: if the list is empty or does not contain the data.

        Examples:
            >>> print(l1)               |   >>> print(l2)
            ['a', 'e', 'i', 'o', 'u']   |   [2, 4, 6, 8]
            >>> l1.remove('i')          |   >>> l2.remove(10)
            >>> l1.remove('a')          |   ValueError: <message>
            >>> l1.remove('u')          |   >>> l3 = LinkedList()
            >>> print(l1)               |   >>> l3.remove(10)
            ['e', 'o']                  |   ValueError: <message>
        """
        # index=0
        if len(self)==0:
            raise ValueError("list is empty")
        
        temp=self.find(data)
        
        node=self.head
        while True:
            if node.value==data:
                if node==self.head:
                    self.head=node.next
                    node.next.prev=None
                    break
                elif node==self.tail:
                    self.tail=node.prev
                    node.prev.next=None
                    break
                else:
            
                    node.prev.next=temp.next
                    node.next.prev=temp.prev
                    break
            
            node=node.next
        self.len-=1

    # Problem 5
    def insert(self, index, data):
        """Insert a node containing data into the list immediately before the
        node at the index-th location.

        Raises:
            IndexError: if index is negative or strictly greater than the
                current number of nodes.

        Examples:
            >>> print(l1)               |   >>> len(l2)
            ['b']                       |   5
            >>> l1.insert(0, 'a')       |   >>> l2.insert(6, 'z')
            >>> print(l1)               |   IndexError: <message>
            ['a', 'b']                  |
            >>> l1.insert(2, 'd')       |   >>> l3 = LinkedList()
            >>> print(l1)               |   >>> l3.insert(1, 'a')
            ['a', 'b', 'd']             |   IndexError: <message>
            >>> l1.insert(2, 'c')       |
            >>> print(l1)               |
            ['a', 'b', 'c', 'd']        |
        """
        
        if index>len(self) or index<0:
            raise IndexError("Index value must be positive and less than or equal to the length of the list")
        
        indexnode=self.get(index)
        newnode=LinkedListNode(data)
        
        if index==len(self):
            self.append(data)
            # return self
        
        elif index==0:
            newnode.prev=None
            newnode.next=indexnode
            indexnode.prev=newnode
            self.head=newnode
            self.len+=1
            # return self
        # tempnode=indexnode
        else:
            newnode.prev=indexnode.prev
            newnode.next=indexnode
            indexnode.prev.next=newnode
            indexnode.prev=newnode
            self.len+=1

# Problem 6: Deque class.

class Deque(LinkedList):
    
    def __init__(self):
        """Initialize the head and tail attributes by setting
        them to None, since the list is empty initially.
        """
        LinkedList.__init__(self)
        # self.head = None
        # self.tail = None
        # self.len = 0
        
    def pop(self):
        """
        Returns:
            the last element of the deque and removes it.
        
        Raises:
            ValueError if the list is empty
        """
        
        if len(self)==0:
            raise ValueError("deque is empty")
        
        elif len(self)==1:
            popped=self.head
            self.head=None
            self.tail=None
            self.len=0
            return popped.value
          
        else:
            popped=self.tail
            previous=popped.prev
            previous.next=None
            self.tail=previous
            self.len-=1
            return popped.value
    
    # def pop2(self):
    #     """
    #     Returns:
    #         the last element of the deque and removes it.
        
    #     Raises:
    #         ValueError if the list is empty
    #         """
        
    #     if len(self)==0:
    #         raise ValueError("deque is empty")
        
    #     if len(self)==1:
    #         popped=self.head
    #         self=Deque()
    #         return popped
            
    #     popped=self.tail
    #     tempnode=self.tail
    #     self.tail.prev.next=None
    #     tempnode.prev=self.tail
    #     self.len-=1
    #     return self
    
    
    def popleft(self):
        
        if len(self)==0:
            raise ValueError("deque is empty")
        
        if len(self)==1:
            popped=self.head
            self=Deque()
            return popped
        
        popped=self.head
        data=self.head.value
        self.remove(data)
        
        return popped.value
        
    def appendleft(self,data):
        self.insert(0,data)
        
    
    def remove(*args, **kwargs):
        raise NotImplementedError("Use pop() or popleft() for removal")

    def insert(*args, **kwargs):
        raise NotImplementedError("Use pop() or popleft() for removal")    
        
# Problem 7
def prob7(infile, outfile):
    """Reverse the contents of a file by line and write the results to
    another file.

    Parameters:
        infile (str): the file to read from.
        outfile (str): the file to write to.
    """
    while True:
            try:
                with open(infile) as infile:
                    contents=infile.read()
                break  
            except (FileNotFoundError, FileExistsError, IOError, SyntaxError):
                infile=input("Please input a valid filename:")
    
    stack=Deque()
    with open(outfile,'w+') as f:
        lines = contents.strip('\n').split('\n')
        # print(lines)
        timer=0
        for i in lines:
            if timer%10000==0:
                print(timer,i)
            stack.append(i)
            # print(stack.find(i).value)
            timer+=1
        timer=0
        print("start writing")
        print(stack.find('purple').value)
        print(len(stack))
        print(stack.pop())
        print(len(stack))
        print(stack.pop())
        print(len(stack))
        while True:
            # print(len1(stack))
            k=stack.pop()
            # print(k)
            # print(len(stack))
            f.write(str(k))
            
            f.write('\n')
            
            if timer%10000==0:
                print(len(stack),timer,k)
            # f.write('\n')
            # tempnode=stack.tail
            # stack.tail.prev.next=None
            # tempnode.prev=stack.tail
            # stack.len-=1
            # stack=stack.pop2()
            # stack.pop()
            if len(stack)==0:
                break
            timer+=1



    
def testlist():
    
    Z=LinkedList()
    Z.append('a')
    Z.append('b')
    Z.append('c')
    Z.append('d')
    Z.append('e')

    return Z

def testdeque():
       
    Z=Deque()
    Z.append('apple')
    Z.append('banana')
    Z.append('carrot')
    Z.append('date')
    Z.append('estuary')
    
    return Z

# prob7('english.txt','englishinvert1.txt')