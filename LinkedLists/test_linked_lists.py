# -*- coding: utf-8 -*-
"""
Created on Thu Sep 30 09:28:23 2021

@author: Broderik
"""

import linked_lists
import pytest


@pytest.fixture
def set_up_node():
    data1=linked_lists.Node('a')
    data2=linked_lists.Node(1)
    data3=linked_lists.Node(1.2)
    return data1, data2, data3

@pytest.fixture
def set_up_linkedlist():
    list1=linked_lists.LinkedList()
    list1.append('a')
    list1.append('b')
    list1.append('c')
    return list1

def test_node_init(set_up_node):
    data1, data2, data3 = set_up_node
    with pytest.raises(TypeError) as exinfo:
        linked_lists.LinkedListNode([1,2,3])
    assert exinfo.value.args[0]=="Data must be a integer, float, or string"
    with pytest.raises(TypeError) as exinfo:
        linked_lists.LinkedListNode({1,2,3})
    assert exinfo.value.args[0]=="Data must be a integer, float, or string"
    assert data1==linked_lists.Node('a')
    
# def test_linkedlistnode_append(set_up_node):
#     assert data1.append


# def test_node_find(set_up_linkedlist):
#     list1=set_up_linkedlist
#     assert list1.find('a') == linked_lists.LinkedListNode('a')
#     with pytest.raises(ValueError) as exinfo:
#         list1.find('d')
#     assert exinfo.value.args[0]== "data is not in the linked list"
    