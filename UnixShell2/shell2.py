# shell2.py
"""Volume 3: Unix Shell 2.
<Name>
<Class>
<Date>
"""
import os 
from glob import glob
import subprocess

# Problem 3
def grep(target_string, file_pattern):
    """Find all files in the current directory or its subdirectories that
    match the file pattern, then determine which ones contain the target
    string.

    Parameters:
        target_string (str): A string to search for in the files whose names
            match the file_pattern.
        file_pattern (str): Specifies which files to search.
    """
    filelist = []
    
    file_type = "**/*" + file_pattern
    filematches = glob(file_type, recursive = True)
    print(filematches)
    for file in filematches:
        with open(file) as contents:
            if target_string in contents.read():
                filelist.append(contents.name)
    
    return filelist


# Problem 4
def largest_files(n):
    """Return a list of the n largest files in the current directory or its
    subdirectories (from largest to smallest).
    """
    
    names_and_sizes = []
    filenames = glob('**/*.*', recursive = True)
    for file in filenames:
        size = os.path.getsize(file)
        names_and_sizes.append((file,size))
    names_and_sizes.sort(key = lambda x : x[1], reverse=True)
    names_and_sizes = names_and_sizes[:n]
    smallest = names_and_sizes[-1][0]
    print(names_and_sizes)
    subprocess.Popen(['touch smallest.txt | wc -l < ' + smallest + ' > smallest.txt'], shell = True)
    return [names_and_sizes[n][0] for n in range(len(names_and_sizes))]    
    
# Problem 6    
def prob6(n = 10):
   """this problem counts to or from n three different ways, and
      returns the resulting lists each integer
   
   Parameters:
       n (int): the integer to count to and down from
   Returns:
       integerCounter (list): list of integers from 0 to the number n
       twoCounter (list): list of integers created by counting down from n by two
       threeCounter (list): list of integers created by counting up to n by 3
   """
   #print what the program is doing
   integerCounter = list()
   twoCounter = list()
   threeCounter = list()
   counter = n
   for i in range(n+1):
       integerCounter.append(i)
       if (i % 2 == 0):
           twoCounter.append(counter - i)
       if (i % 3 == 0):
           threeCounter.append(i)
   #return relevant values
   return integerCounter, twoCounter, threeCounter
